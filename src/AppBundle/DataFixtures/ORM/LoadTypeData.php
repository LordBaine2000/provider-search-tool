<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\AppBundle;
use AppBundle\Entity\AncillaryType;
use AppBundle\Entity\CoreType;
use AppBundle\Entity\FacilitySubtype;
use AppBundle\Entity\FacilityType;
use AppBundle\Entity\GenderType;
use AppBundle\Entity\LanguageType;
use AppBundle\Entity\PlanType;
use AppBundle\Entity\ProviderFlagType;
use AppBundle\Entity\ProviderType;
use AppBundle\Entity\SpecialtyType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Loads initial application data
 */
class LoadTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    const NAME       = 'LoadTypeData';
    const NAME_SPACE = AppBundle::DATA_FIXTURES_ORM_NAMESPACE.self::NAME;


    private static $types = [
        PlanType::NAME_SPACE      => [
            PlanType::HMO_SLUG      => 'HMO',
            PlanType::PPO_SLUG      => 'PPO',
            PlanType::EPO_SLUG      => 'EPO',
            PlanType::EPP_SLUG      => 'EPP',
            PlanType::MEDICAID_SLUG => 'Medicaid',
            PlanType::CHILD_SLUG    => 'Child Health Plus',
            PlanType::BASIC_SLUG    => 'Basic Health Plan'
        ],
        SpecialtyType::NAME_SPACE => [
            SpecialtyType::FAMILY_SLUG          => 'Family Practice',
            SpecialtyType::GENERAL_SLUG         => 'General Medicine',
            SpecialtyType::INTERNAL_SLUG        => 'Internal Medicine',
            SpecialtyType::PEDIATRICS_SLUG      => 'Pediatrics',
            SpecialtyType::OBGYN_SLUG           => 'OBGYN (Obstetrics/Gynecology)',
            SpecialtyType::ALL_PRIMARY_SLUG     => 'All Primary Care Physicians',
            SpecialtyType::ALLERGY_SLUG         => 'Allergy/Immunology'
        ],
        FacilityType::NAME_SPACE  => [
            FacilityType::DENTAL_SLUG     => 'Dental Care Provider',
            FacilityType::VISION_SLUG     => 'Vision Care Provider',
            FacilityType::LAB_SLUG        => 'Lab',
            FacilityType::PHARMACY_SLUG   => 'Pharmacy',
        ],
        FacilitySubtype::NAME_SPACE  => [
            FacilitySubtype::HOSPITAL_SLUG   => 'Hospital',
            FacilitySubtype::URGENT_SLUG     => 'Urgent Care Center',
            FacilitySubtype::DURABLE_SLUG    => 'Durable Medical Equipment (DME)',
            FacilitySubtype::BEHAVIORAL_SLUG => 'Behavioral Health',
            FacilitySubtype::DIAGNOSTIC_SLUG => 'Diagnostic Center',
            FacilitySubtype::OTHER_SLUG      => 'Other Facility Type',
        ],
        AncillaryType::NAME_SPACE => [
            AncillaryType::HOME_SLUG    => 'Home Health',
            AncillaryType::HOSPICE_SLUG => 'Hospice',
            AncillaryType::OTHER_SLUG   => 'Other Ancillary Services'
        ],
        ProviderType::NAME_SPACE  => [
            ProviderType::PRIMARY_SLUG   => 'Primary Care Physician or Specialist',
            ProviderType::HOSPITAL_SLUG  => 'Hospital, Facility or Urgent Care Center',
            ProviderType::ANCILLARY_SLUG => 'Ancillary Provider'
        ],
        CoreType::NAME_SPACE => [
            CoreType::A,
            CoreType::B,
            CoreType::C
        ],
        GenderType::NAME_SPACE => [
            GenderType::MALE,
            GenderType::FEMALE
        ],
        LanguageType::NAME_SPACE => [
            'English',
            'Spanish'
        ],
        ProviderFlagType::NAME_SPACE => [
            ProviderFlagType::ACCEPTING_PATIENTS,
            ProviderFlagType::CERTIFIED
        ]
    ];


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach($this::$types as $class => $data)
        {
            foreach($data as $slug => $name)
            {
                if(class_exists($class) && method_exists($entity = new $class(), 'setName'))
                {
                    $entity->setName($name);

                    if($slugType = method_exists($entity, 'setSlug'))
                    {
                        $entity->setSlug($slug);
                    }

                    $manager->persist($entity);
                    $this->addReference(
                        ($slugType) ? "$class::$slug" : "$class::$name",
                        $entity
                    );
                }
            }
        }

        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 1;
    }
}
