<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\AppBundle;
use AppBundle\Entity as Entity;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Loads fake data for testing
 *
 * @TODO: Implement environment specific fixtures
 * @see http://stackoverflow.com/questions/11817971/environment-specific-data-fixtures-with-symfonydoctrine#answer-21169675
 */
class LoadTestData extends AbstractFixture implements OrderedFixtureInterface
{
    const NAME       = 'LoadTestData';
    const NAME_SPACE = AppBundle::DATA_FIXTURES_ORM_NAMESPACE.self::NAME;


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $hours = new Entity\HoursType();
        $hours->setName('Test HoursType');
        $manager->persist($hours);

        $website = new Entity\WebsiteType();
        $website->setUrl('www.test.com');
        $manager->persist($website);

        $city = new Entity\CityType();
        $city->setName('Farmington');
        $manager->persist($city);

        $iso = new Entity\RegionIsoCodeType();
        $iso->setAlpha2Code('UT');
        $manager->persist($iso);

        $region = new Entity\Region();
        $region
            ->setName('Utah')
            ->setIsoCode($iso)
        ;
        $manager->persist($region);

        $postcode = new Entity\Postcode();
        $postcode->setName('84025');
        $manager->persist($postcode);

        $addressFlag = new Entity\AddressFlagType();
        $addressFlag->setName('Test AddressFlagType');
        $manager->persist($addressFlag);

        $phoneType = new Entity\PhoneType();
        $phoneType->setName('Test PhoneType');
        $manager->persist($phoneType);

        $phone = new Entity\Phone();
        $phone
            ->setNumber(11231231234)
            ->setType($phoneType)
        ;
        $manager->persist($phone);

        $address = new Entity\Address();
        $address
            ->setStreet1('240 S 200 W')
            ->setCity($city)
            ->setPostCode($postcode)
            ->setRegion($region)
            ->setHours($hours)
            ->addFlag($addressFlag)
            ->addPhone($phone)
        ;
        $manager->persist($address);

        $address2 = new Entity\Address();
        $address2
            ->setStreet1('697 Lagoon Dr')
            ->setCity($city)
            ->setPostCode($postcode)
            ->setRegion($region)
            ->setHours($hours)
            ->addFlag($addressFlag)
            ->addPhone($phone)
        ;
        $manager->persist($address2);

        $address3 = new Entity\Address();
        $address3
            ->setStreet1('1282 W 1875 N')
            ->setCity($city)
            ->setPostCode($postcode)
            ->setRegion($region)
            ->setHours($hours)
            ->addFlag($addressFlag)
            ->addPhone($phone)
        ;
        $manager->persist($address3);

        $domain = new Entity\DomainType();
        $domain->setName('test.com');
        $manager->persist($domain);

        $email = new Entity\Email();
        $email
            ->setUsername('test')
            ->setDomain($domain)
        ;
        $manager->persist($email);

        $ancillary = new Entity\Ancillary();
        $ancillary
            ->setName('Test Ancillary 1')
            ->setType($this->getReference('AppBundle\Entity\AncillaryType::'.Entity\AncillaryType::HOSPICE_SLUG))
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::CERTIFIED))
            ->addAddress($address)
            ->addEmail($email)
        ;
        $manager->persist($ancillary);

        $ancillary2 = new Entity\Ancillary();
        $ancillary2
            ->setName('Test Ancillary 2')
            ->setType($this->getReference('AppBundle\Entity\AncillaryType::'.Entity\AncillaryType::HOSPICE_SLUG))
            ->setCoreType($this->getReference('AppBundle\Entity\CoreType::'.Entity\CoreType::B))
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::CERTIFIED))
            ->addAddress($address2)
            ->addEmail($email)
        ;
        $manager->persist($ancillary2);

        $ancillary3 = new Entity\Ancillary();
        $ancillary3
            ->setName('Test Ancillary 3')
            ->setType($this->getReference('AppBundle\Entity\AncillaryType::'.Entity\AncillaryType::HOSPICE_SLUG))
            ->setCoreType($this->getReference('AppBundle\Entity\CoreType::'.Entity\CoreType::A))
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::CERTIFIED))
            ->addAddress($address3)
            ->addEmail($email)
        ;
        $manager->persist($ancillary3);

        $facility = new Entity\Facility();
        $facility
            ->setName('Test Facility 1')
            ->setType($this->getReference('AppBundle\Entity\FacilitySubtype::'.Entity\FacilitySubtype::BEHAVIORAL_SLUG))
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::ACCEPTING_PATIENTS))
            ->addAddress($address)
            ->addEmail($email)
        ;
        $manager->persist($facility);

        $facility2 = new Entity\Facility();
        $facility2
            ->setName('Test Facility 2')
            ->setType($this->getReference('AppBundle\Entity\FacilitySubtype::'.Entity\FacilitySubtype::BEHAVIORAL_SLUG))
            ->setCoreType($this->getReference('AppBundle\Entity\CoreType::'.Entity\CoreType::B))
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::CERTIFIED))
            ->addAddress($address2)
            ->addEmail($email)
        ;
        $manager->persist($facility2);

        $facility3 = new Entity\Facility();
        $facility3
            ->setName('Test Facility 3')
            ->setType($this->getReference('AppBundle\Entity\FacilitySubtype::'.Entity\FacilitySubtype::BEHAVIORAL_SLUG))
            ->setCoreType($this->getReference('AppBundle\Entity\CoreType::'.Entity\CoreType::A))
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::CERTIFIED))
            ->addAddress($address3)
            ->addEmail($email)
        ;
        $manager->persist($facility3);

        $lab = new Entity\Facility();
        $lab
            ->setName('Test Lab')
            ->setType($this->getReference('AppBundle\Entity\FacilityType::'.Entity\FacilityType::LAB_SLUG))
            ->setCoreType($this->getReference('AppBundle\Entity\CoreType::'.Entity\CoreType::A))
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::ACCEPTING_PATIENTS))
            ->addAddress($address3)
            ->addEmail($email)
        ;
        $manager->persist($lab);

        $affiliationType = new Entity\AffiliationType();
        $affiliationType->setName('Test AffiliationType');
        $manager->persist($affiliationType);

        $affiliation = new Entity\Affiliation();
        $affiliation
            ->setName('Test Affiliation')
            ->setType($affiliationType)
            ->addAddress($address)
        ;
        $manager->persist($affiliation);

        $degree = new Entity\DegreeType();
        $degree->setName('Test DegreeType');
        $manager->persist($degree);

        $person = new Entity\Person();
        $person
            ->setFirstName('Test')
            ->setLastName('Person 1')
            ->setGender($this->getReference('AppBundle\Entity\GenderType::'.Entity\GenderType::MALE))
            ->addSpecialty($this->getReference('AppBundle\Entity\SpecialtyType::'.Entity\SpecialtyType::OBGYN_SLUG))
            ->addLanguage($this->getReference('AppBundle\Entity\LanguageType::English'))
            ->addAffiliation($affiliation)
            ->addDegree($degree)
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::CERTIFIED))
            ->addAddress($address)
            ->addEmail($email)
        ;
        $manager->persist($person);

        $person2 = new Entity\Person();
        $person2
            ->setFirstName('Test')
            ->setLastName('Person 2')
            ->setGender($this->getReference('AppBundle\Entity\GenderType::'.Entity\GenderType::MALE))
            ->addSpecialty($this->getReference('AppBundle\Entity\SpecialtyType::'.Entity\SpecialtyType::OBGYN_SLUG))
            ->setCoreType($this->getReference('AppBundle\Entity\CoreType::'.Entity\CoreType::B))
            ->addLanguage($this->getReference('AppBundle\Entity\LanguageType::English'))
            ->addAffiliation($affiliation)
            ->addDegree($degree)
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::CERTIFIED))
            ->addAddress($address2)
            ->addEmail($email)
        ;
        $manager->persist($person2);

        $person3 = new Entity\Person();
        $person3
            ->setFirstName('Test')
            ->setLastName('Person 3')
            ->setGender($this->getReference('AppBundle\Entity\GenderType::'.Entity\GenderType::FEMALE))
            ->addSpecialty($this->getReference('AppBundle\Entity\SpecialtyType::'.Entity\SpecialtyType::OBGYN_SLUG))
            ->setCoreType($this->getReference('AppBundle\Entity\CoreType::'.Entity\CoreType::A))
            ->addLanguage($this->getReference('AppBundle\Entity\LanguageType::Spanish'))
            ->addAffiliation($affiliation)
            ->addDegree($degree)
            ->setWebsite($website)
            ->addAcceptedPlan($this->getReference('AppBundle\Entity\PlanType::'.Entity\PlanType::MEDICAID_SLUG))
            ->addFlag($this->getReference('AppBundle\Entity\ProviderFlagType::'.Entity\ProviderFlagType::ACCEPTING_PATIENTS))
            ->addAddress($address3)
            ->addEmail($email)
        ;
        $manager->persist($person3);

        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 2;
    }
}
