<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use CrEOF\Spatial\PHP\Types\Geography\Point;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * LocationType Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class LocationType
{
    const NAME        = 'LocationType';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var Point
     *
     * @ORM\Column(type="point", unique=true)
     */
    private $point;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addresses = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get point
     *
     * @return Point
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set point
     *
     * @param Point $point
     * @return $this
     */
    public function setPoint(Point $point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get addresses
     *
     * @return ArrayCollection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Set addresses
     *
     * @param ArrayCollection $addresses
     * @return $this
     */
    public function setAddresses(ArrayCollection $addresses)
    {
        $this->addresses = $addresses;

        return $this;
    }

    /**
     * Add address
     *
     * @param Address $address
     * @return $this
     */
    public function addAddress(Address $address)
    {
        $this->addresses->add($address);

        return $this;
    }

    /**
     * Remove address
     *
     * @param Address $address
     * @return $this
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);

        return $this;
    }
}
