<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use \AppBundle\Model\TypeTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * CoreType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class CoreType
{
    const NAME        = 'CoreType';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;

    const A = 'A';
    const B = 'B';
    const C = 'C';


    use TypeTrait;
}
