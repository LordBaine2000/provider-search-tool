<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Facility Entity
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(columns={"name"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FacilityRepository")
 */
class Facility extends Provider
{
    const NAME        = 'Facility';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=125)
     *
     * @Assert\NotBlank(message = "label.error.name_blank")
     */
    private $name;

    /**
     * @var FacilityType
     *
     * @ORM\ManyToOne(targetEntity="FacilityType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;


    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get type
     *
     * @return FacilityType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param FacilityType $type
     * @return $this
     */
    public function setType(FacilityType $type)
    {
        $this->type = $type;

        return $this;
    }
}
