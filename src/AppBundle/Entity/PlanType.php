<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use \AppBundle\Model\SlugTypeTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * PlanType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class PlanType
{
    const NAME          = 'PlanType';
    const NAME_SPACE    = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME   = AppBundle::NAME.':'.self::NAME;

    const HMO_SLUG      = 'hmo';
    const PPO_SLUG      = 'ppo';
    const EPO_SLUG      = 'epo';
    const EPP_SLUG      = 'epp';
    const MEDICAID_SLUG = 'medicaid';
    const CHILD_SLUG    = 'child-health-plus';
    const BASIC_SLUG    = 'basic-health-plan';


    use SlugTypeTrait;
}
