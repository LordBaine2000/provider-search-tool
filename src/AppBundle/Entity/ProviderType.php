<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use AppBundle\Model\SlugTypeTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProviderType Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProviderTypeRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "SpecialtyType"   = "SpecialtyType",
 *     "FacilityType"    = "FacilityType",
 *     "FacilitySubtype" = "FacilitySubtype",
 *     "AncillaryType"   = "AncillaryType",
 *     "ProviderType"    = "ProviderType"
 * })
 */
class ProviderType
{
    const NAME           = 'ProviderType';
    const NAME_SPACE     = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME    = AppBundle::NAME.':'.self::NAME;

    const PRIMARY_SLUG   = 'primary-care-or-specialist';
    const HOSPITAL_SLUG  = 'hospital-facility-or-urgent-care';
    const ANCILLARY_SLUG = 'ancillary';


    use SlugTypeTrait;
}
