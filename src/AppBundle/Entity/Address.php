<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use AppBundle\Model\LocationInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Address Entity
 *
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"street1", "street2", "city_id", "region_id", "post_code_id"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class Address implements LocationInterface
{
    const NAME        = 'Address';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="street1", type="string", length=125)
     *
     * @Assert\NotBlank(message = "label.error.street1_blank")
     */
    private $street1;

    /**
     * @var string
     *
     * @ORM\Column(name="street2", type="string", length=125, nullable=true)
     */
    private $street2;

    /**
     * @var CityType
     *
     * @ORM\ManyToOne(targetEntity="CityType")
     * @ORM\JoinColumn(name="city_id", nullable=false)
     */
    private $city;

    /**
     * @var Region
     *
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="region_id", nullable=false)
     */
    private $region;

    /**
     * @var Postcode
     *
     * @ORM\ManyToOne(targetEntity="Postcode")
     * @ORM\JoinColumn(name="post_code_id", nullable=false)
     */
    private $postCode;

    /**
     * @var ArrayCollection<AddressFlagType>
     *
     * @ORM\ManyToMany(targetEntity="AddressFlagType")
     */
    private $flags;

    /**
     * @var ArrayCollection<Phone>
     *
     * @ORM\ManyToMany(targetEntity="Phone")
     */
    private $phones;

    /**
     * @var ArrayCollection<HoursType>
     *
     * @ORM\ManyToMany(targetEntity="HoursType")
     * @ORM\JoinTable(
     *     name="address_hours_type",
     *     joinColumns={
     *         @ORM\JoinColumn(name="address_id", referencedColumnName="id", unique=true)
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="hours_type_id", referencedColumnName="id")
     *     }
     * )
     */
    private $hours;

    /**
     * @var ArrayCollection<LocationType>
     *
     * @ORM\ManyToMany(targetEntity="LocationType")
     * @ORM\JoinTable(
     *     name="address_location_type",
     *     joinColumns={
     *         @ORM\JoinColumn(name="address_id", referencedColumnName="id", unique=true)
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="location_type_id", referencedColumnName="id")
     *     }
     * )
     */
    private $location;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flags    = new ArrayCollection();
        $this->phones   = new ArrayCollection();
        $this->hours    = new ArrayCollection();
        $this->location = new ArrayCollection();
    }

    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s %s, %s %s',
            ($this->getStreet2()) ? $this->getStreet1().' '.$this->getStreet2() : $this->getStreet1(),
            $this->getCity(),
            $this->getRegion(),
            $this->getPostCode()
        );
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get street1
     *
     * @return string
     */
    public function getStreet1()
    {
        return $this->street1;
    }

    /**
     * Set street1
     *
     * @param string $street1
     * @return $this
     */
    public function setStreet1($street1)
    {
        $this->street1 = $street1;

        return $this;
    }

    /**
     * Get street2
     *
     * @return string
     */
    public function getStreet2()
    {
        return $this->street2;
    }

    /**
     * Set street2
     *
     * @param string $street2
     * @return $this
     */
    public function setStreet2($street2)
    {
        $this->street2 = $street2;

        return $this;
    }

    /**
     * Get city
     *
     * @return CityType
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param CityType $city
     * @return $this
     */
    public function setCity(CityType $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get region
     *
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set region
     *
     * @param Region $region
     * @return $this
     */
    public function setRegion(Region $region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get postCode
     *
     * @return Postcode
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set postCode
     *
     * @param Postcode $postCode
     * @return $this
     */
    public function setPostCode(Postcode $postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get flags
     *
     * @return ArrayCollection
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * Set flags
     *
     * @param ArrayCollection $flags
     * @return $this
     */
    public function setFlags(ArrayCollection $flags)
    {
        $this->flags = $flags;

        return $this;
    }

    /**
     * Add flag
     *
     * @param AddressFlagType $flag
     * @return $this
     */
    public function addFlag(AddressFlagType $flag)
    {
        $this->flags->add($flag);

        return $this;
    }

    /**
     * Remove flag
     *
     * @param AddressFlagType $flag
     * @return $this
     */
    public function removeFlag(AddressFlagType $flag)
    {
        $this->flags->removeElement($flag);

        return $this;
    }

    /**
     * Get phones
     *
     * @return ArrayCollection
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set phones
     *
     * @param ArrayCollection $phones
     * @return $this
     */
    public function setPhones(ArrayCollection $phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Add phone
     *
     * @param Phone $phone
     * @return $this
     */
    public function addPhone(Phone $phone)
    {
        $this->phones->add($phone);

        return $this;
    }

    /**
     * Remove phone
     *
     * @param Phone $phone
     * @return $this
     */
    public function removePhone(Phone $phone)
    {
        $this->phones->removeElement($phone);

        return $this;
    }

    /**
     * Get hours
     *
     * @return HoursType
     */
    public function getHours()
    {
        return $this->hours->first();
    }

    /**
     * Set hours
     *
     * @param HoursType $hours
     * @return $this
     */
    public function setHours(HoursType $hours)
    {
        $this->hours->clear();
        $this->hours->add($hours);

        return $this;
    }

    /**
     * Get location
     *
     * @return LocationType
     */
    public function getLocation()
    {
        return $this->location->first();
    }

    /**
     * Set location
     *
     * @param LocationType $location
     * @return $this
     */
    public function setLocation(LocationType $location)
    {
        $this->location->clear();
        $this->location->add($location);

        return $this;
    }
}
