<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * AncillaryType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class AncillaryType extends ProviderType
{
    const NAME         = 'AncillaryType';
    const NAME_SPACE   = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME  = AppBundle::NAME.':'.self::NAME;

    const HOME_SLUG    = 'home-health';
    const HOSPICE_SLUG = 'hospice';
    const OTHER_SLUG   = 'other-ancillary-services';
}
