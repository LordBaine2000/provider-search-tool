<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Person Entity
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(columns={"first_name", "last_name"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 */
class Person extends Provider
{
    const NAME        = 'Person';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=125)
     *
     * @Assert\NotBlank(message = "label.error.first_name_blank")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=125, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=125)
     *
     * @Assert\NotBlank(message = "label.error.last_name_blank")
     */
    private $lastName;

    /**
     * @var GenderType
     *
     * @ORM\ManyToOne(targetEntity="GenderType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $gender;

    /**
     * @var ArrayCollection<SpecialtyType>
     *
     * @ORM\ManyToMany(targetEntity="SpecialtyType")
     */
    private $specialties;

    /**
     * @var ArrayCollection<LanguageType>
     *
     * @ORM\ManyToMany(targetEntity="LanguageType")
     */
    private $languages;

    /**
     * @var ArrayCollection<Affiliation>
     *
     * @ORM\ManyToMany(targetEntity="Affiliation")
     */
    private $affiliations;

    /**
     * @var ArrayCollection<DegreeType>
     *
     * @ORM\ManyToMany(targetEntity="DegreeType")
     */
    private $degrees;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->specialties  = new ArrayCollection();
        $this->languages    = new ArrayCollection();
        $this->affiliations = new ArrayCollection();
        $this->degrees      = new ArrayCollection();
    }

    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get full name
     *
     * @return string
     */
    public function getName()
    {
        return implode(' ', [
            $this->getFirstName(),
            $this->getMiddleName(),
            $this->getLastName()
        ]);
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return $this
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get gender
     *
     * @return GenderType
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set gender
     *
     * @param GenderType $gender
     * @return $this
     */
    public function setGender(GenderType $gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get specialties
     *
     * @return ArrayCollection<SpecialtyType>
     */
    public function getSpecialties()
    {
        return $this->specialties;
    }

    /**
     * Set specialties
     *
     * @param ArrayCollection $specialties
     * @return $this
     */
    public function setSpecialties(ArrayCollection $specialties)
    {
        $this->specialties = $specialties;

        return $this;
    }

    /**
     * Add specialty
     *
     * @param SpecialtyType $specialty
     * @return $this
     */
    public function addSpecialty(SpecialtyType $specialty)
    {
        $this->specialties->add($specialty);

        return $this;
    }

    /**
     * Remove specialty
     *
     * @param SpecialtyType $specialty
     * @return $this
     */
    public function removeSpecialty(SpecialtyType $specialty)
    {
        $this->specialties->removeElement($specialty);

        return $this;
    }

    /**
     * Get languages
     *
     * @return ArrayCollection<LanguageType>
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set languages
     *
     * @param ArrayCollection $languages
     * @return $this
     */
    public function setLanguages(ArrayCollection $languages)
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * Add language
     *
     * @param LanguageType $language
     * @return $this
     */
    public function addLanguage(LanguageType $language)
    {
        $this->languages->add($language);

        return $this;
    }

    /**
     * Remove language
     *
     * @param LanguageType $language
     * @return $this
     */
    public function removeLanguage(LanguageType $language)
    {
        $this->languages->removeElement($language);

        return $this;
    }

    /**
     * Get affiliations
     *
     * @return ArrayCollection<AffiliationType>
     */
    public function getAffiliations()
    {
        return $this->affiliations;
    }

    /**
     * Set affiliations
     *
     * @param ArrayCollection $affiliations
     * @return $this
     */
    public function setAffiliations(ArrayCollection $affiliations)
    {
        $this->affiliations = $affiliations;

        return $this;
    }

    /**
     * Add affiliation
     *
     * @param Affiliation $affiliation
     * @return $this
     */
    public function addAffiliation(Affiliation $affiliation)
    {
        $this->affiliations->add($affiliation);

        return $this;
    }

    /**
     * Remove affiliation
     *
     * @param Affiliation $affiliation
     * @return $this
     */
    public function removeAffiliation(Affiliation $affiliation)
    {
        $this->affiliations->removeElement($affiliation);

        return $this;
    }

    /**
     * Get degrees
     *
     * @return ArrayCollection<DegreeType>
     */
    public function getDegrees()
    {
        return $this->degrees;
    }

    /**
     * Set degrees
     *
     * @param ArrayCollection $degrees
     * @return $this
     */
    public function setDegrees(ArrayCollection $degrees)
    {
        $this->degrees = $degrees;

        return $this;
    }

    /**
     * Add degree
     *
     * @param DegreeType $degree
     * @return $this
     */
    public function addDegree(DegreeType $degree)
    {
        $this->degrees->add($degree);

        return $this;
    }

    /**
     * Remove degree
     *
     * @param DegreeType $degree
     * @return $this
     */
    public function removeDegree(DegreeType $degree)
    {
        $this->degrees->removeElement($degree);

        return $this;
    }
}
