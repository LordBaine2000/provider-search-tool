<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use AppBundle\Model\LocationInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Postcode Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class Postcode implements LocationInterface
{
    const NAME        = 'Postcode';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=125, unique=true)
     *
     * @Assert\NotBlank(message = "label.error.name_blank")
     */
    private $name;

    /**
     * @var ArrayCollection<LocationType>
     *
     * @ORM\ManyToMany(targetEntity="LocationType")
     * @ORM\JoinTable(
     *     name="postcode_location_type",
     *     joinColumns={
     *         @ORM\JoinColumn(name="postcode_id", referencedColumnName="id", unique=true)
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="location_type_id", referencedColumnName="id")
     *     }
     * )
     */
    private $location;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->location = new ArrayCollection();
    }

    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get location
     *
     * @return LocationType
     */
    public function getLocation()
    {
        return $this->location->first();
    }

    /**
     * Set location
     *
     * @param LocationType $location
     * @return $this
     */
    public function setLocation(LocationType $location)
    {
        $this->location->clear();
        $this->location->add($location);

        return $this;
    }
}
