<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * SpecialtyType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class SpecialtyType extends ProviderType
{
    const NAME        = 'SpecialtyType';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;

    const FAMILY_SLUG          = 'family-practice';
    const GENERAL_SLUG         = 'general-medicine';
    const INTERNAL_SLUG        = 'internal-medicine';
    const PEDIATRICS_SLUG      = 'pediatrics';
    const OBGYN_SLUG           = 'obgyn-obstetrics-gynecology';
    const ALL_PRIMARY_SLUG     = 'all-primary-care-physicians';
    const ALLERGY_SLUG         = 'allergy-immunology';
}
