<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use \AppBundle\Model\TypeTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * DegreeType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class DegreeType
{
    const NAME        = 'DegreeType';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    use TypeTrait;
}
