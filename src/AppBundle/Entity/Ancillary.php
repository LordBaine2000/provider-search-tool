<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ancillary Entity
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(columns={"name"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AncillaryRepository")
 */
class Ancillary extends Provider
{
    const NAME        = 'Ancillary';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=125)
     *
     * @Assert\NotBlank(message = "label.error.name_blank")
     */
    private $name;

    /**
     * @var AncillaryType
     *
     * @ORM\ManyToOne(targetEntity="AncillaryType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;


    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get type
     *
     * @return AncillaryType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param AncillaryType $type
     * @return $this
     */
    public function setType(AncillaryType $type)
    {
        $this->type = $type;

        return $this;
    }
}
