<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * FacilitySubtype Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class FacilitySubtype extends FacilityType
{
    const NAME            = 'FacilitySubtype';
    const NAME_SPACE      = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME     = AppBundle::NAME.':'.self::NAME;

    const HOSPITAL_SLUG   = 'hospital';
    const URGENT_SLUG     = 'urgent-care-center';
    const DURABLE_SLUG    = 'durable-medical-equipment-dme';
    const BEHAVIORAL_SLUG = 'behavioral-health';
    const DIAGNOSTIC_SLUG = 'diagnostic-center';
    const OTHER_SLUG      = 'other-facility-type';
}
