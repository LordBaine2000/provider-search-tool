<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Provider Entity
 *
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="provider_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "Person"    = "Person",
 *     "Facility"  = "Facility",
 *     "Ancillary" = "Ancillary"
 * })
 */
abstract class Provider
{
    const NAME        = 'Provider';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var ArrayCollection<PlanType>
     *
     * @ORM\ManyToMany(targetEntity="PlanType")
     */
    protected $acceptedPlans;

    /**
     * @var ArrayCollection<ProviderFlagType>
     *
     * @ORM\ManyToMany(targetEntity="ProviderFlagType")
     */
    protected $flags;

    /**
     * @var ArrayCollection<Address>
     *
     * @ORM\ManyToMany(targetEntity="Address")
     */
    private $addresses;

    /**
     * @var ArrayCollection<Email>
     *
     * @ORM\ManyToMany(targetEntity="Email")
     */
    private $emails;

    /**
     * @var ArrayCollection<WebsiteType>
     *
     * @ORM\ManyToMany(targetEntity="WebsiteType")
     * @ORM\JoinTable(
     *     name="provider_website_type",
     *     joinColumns={
     *         @ORM\JoinColumn(name="provider_id", referencedColumnName="id", unique=true)
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="website_type_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $website;

    /**
     * @var ArrayCollection<CoreType>
     *
     * @ORM\ManyToMany(targetEntity="CoreType")
     * @ORM\JoinTable(
     *     name="provider_core_type",
     *     joinColumns={
     *         @ORM\JoinColumn(name="provider_id", referencedColumnName="id", unique=true)
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="core_type_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $coreType;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->acceptedPlans = new ArrayCollection();
        $this->flags         = new ArrayCollection();
        $this->addresses     = new ArrayCollection();
        $this->emails        = new ArrayCollection();
        $this->website       = new ArrayCollection();
        $this->coreType      = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    abstract public function getName();

    /**
     * Get acceptedPlans
     *
     * @return ArrayCollection<PlanType>
     */
    public function getAcceptedPlans()
    {
        return $this->acceptedPlans;
    }

    /**
     * Set acceptedPlans
     *
     * @param ArrayCollection $acceptedPlans
     * @return $this
     */
    public function setAcceptedPlans(ArrayCollection $acceptedPlans)
    {
        $this->acceptedPlans = $acceptedPlans;

        return $this;
    }

    /**
     * Add accepted plan
     *
     * @param PlanType $plan
     * @return $this
     */
    public function addAcceptedPlan(PlanType $plan)
    {
        $this->acceptedPlans->add($plan);

        return $this;
    }

    /**
     * Remove accepted plan
     *
     * @param PlanType $plan
     * @return $this
     */
    public function removeAcceptedPlan(PlanType $plan)
    {
        $this->acceptedPlans->removeElement($plan);

        return $this;
    }

    /**
     * Get flags
     *
     * @return ArrayCollection<ProviderFlagType>
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * Set flags
     *
     * @param ArrayCollection $flags
     * @return $this
     */
    public function setFlags(ArrayCollection $flags)
    {
        $this->flags = $flags;

        return $this;
    }

    /**
     * Add flag
     *
     * @param ProviderFlagType $flag
     * @return $this
     */
    public function addFlag(ProviderFlagType $flag)
    {
        $this->flags->add($flag);

        return $this;
    }

    /**
     * Remove flag
     *
     * @param ProviderFlagType $flag
     * @return $this
     */
    public function removeFlag(ProviderFlagType $flag)
    {
        $this->flags->removeElement($flag);

        return $this;
    }

    /**
     * Get addresses
     *
     * @return ArrayCollection<Address>
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Set addresses
     *
     * @param ArrayCollection $addresses
     * @return $this
     */
    public function setAddresses(ArrayCollection $addresses)
    {
        $this->addresses = $addresses;

        return $this;
    }

    /**
     * Add address
     *
     * @param Address $address
     * @return $this
     */
    public function addAddress(Address $address)
    {
        $this->addresses->add($address);

        return $this;
    }

    /**
     * Remove address
     *
     * @param Address $address
     * @return $this
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);

        return $this;
    }

    /**
     * Get emails
     *
     * @return ArrayCollection<Email>
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Set emails
     *
     * @param ArrayCollection $emails
     * @return $this
     */
    public function setEmails(ArrayCollection $emails)
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * Add email
     *
     * @param Email $email
     * @return $this
     */
    public function addEmail(Email $email)
    {
        $this->emails->add($email);

        return $this;
    }

    /**
     * Remove email
     *
     * @param Email $email
     * @return $this
     */
    public function removeEmail(Email $email)
    {
        $this->emails->removeElement($email);

        return $this;
    }

    /**
     * Get website
     *
     * @return WebsiteType
     */
    public function getWebsite()
    {
        return $this->website->first();
    }

    /**
     * Set website
     *
     * @param WebsiteType $website
     * @return $this
     */
    public function setWebsite(WebsiteType $website)
    {
        $this->website->clear();
        $this->website->add($website);

        return $this;
    }

    /**
     * Get coreType
     *
     * @return CoreType
     */
    public function getCoreType()
    {
        return $this->coreType->first();
    }

    /**
     * Set coreType
     *
     * @param CoreType $coreType
     * @return $this
     */
    public function setCoreType(CoreType $coreType)
    {
        $this->coreType->clear();
        $this->coreType->add($coreType);

        return $this;
    }
}
