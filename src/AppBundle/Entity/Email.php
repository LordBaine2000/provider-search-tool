<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Email Entity
 *
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"username", "domain_id"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class Email
{
    const NAME        = 'Email';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=125)
     *
     * @Assert\NotBlank(message = "label.error.username_blank")
     */
    private $username;

    /**
     * @var DomainType
     *
     * @ORM\ManyToOne(targetEntity="DomainType")
     * @ORM\JoinColumn(name="domain_id", nullable=false)
     */
    private $domain;


    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s@%s',
            $this->getUsername(),
            $this->getDomain()
        );
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get domain
     *
     * @return DomainType
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set domain
     *
     * @param DomainType $domain
     * @return $this
     */
    public function setDomain(DomainType $domain)
    {
        $this->domain = $domain;

        return $this;
    }
}
