<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;

/**
 * Phone Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class Phone
{
    const NAME        = 'Phone';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="bigint")
     */
    private $number;

    /**
     * @var PhoneType
     *
     * @ORM\ManyToOne(targetEntity="PhoneType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;


    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getNumber();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set number
     *
     * @param int $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get type
     *
     * @return PhoneType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param PhoneType $type
     * @return $this
     */
    public function setType(PhoneType $type)
    {
        $this->type = $type;

        return $this;
    }
}
