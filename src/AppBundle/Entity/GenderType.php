<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use \AppBundle\Model\TypeTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * GenderType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class GenderType
{
    const NAME        = 'GenderType';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;

    const MALE   = 'male';
    const FEMALE = 'female';


    use TypeTrait;
}
