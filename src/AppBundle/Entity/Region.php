<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Region Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class Region
{
    const NAME        = 'Region';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=125, unique=true)
     *
     * @Assert\NotBlank(message = "label.error.name_blank")
     */
    private $name;

    /**
     * @var RegionIsoCodeType
     *
     * @ORM\ManyToOne(targetEntity="RegionIsoCodeType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $isoCode;


    /**
     * String conversion
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get isoCode
     *
     * @return RegionIsoCodeType
     */
    public function getIsoCode()
    {
        return $this->isoCode;
    }

    /**
     * Set isoCode
     *
     * @param RegionIsoCodeType $isoCode
     * @return $this
     */
    public function setIsoCode(RegionIsoCodeType $isoCode)
    {
        $this->isoCode = $isoCode;

        return $this;
    }
}
