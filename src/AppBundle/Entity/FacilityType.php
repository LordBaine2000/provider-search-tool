<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * FacilityType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FacilityTypeRepository")
 */
class FacilityType extends ProviderType
{
    const NAME            = 'FacilityType';
    const NAME_SPACE      = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME     = AppBundle::NAME.':'.self::NAME;

    const DENTAL_SLUG    = 'dental';
    const VISION_SLUG    = 'vision';
    const LAB_SLUG       = 'lab';
    const PHARMACY_SLUG  = 'pharmacy';
}
