<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use \AppBundle\Model\TypeTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * CityType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class CityType
{
    const NAME        = 'CityType';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;


    use TypeTrait;
}
