<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use \AppBundle\Model\TypeTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProviderFlagType Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class ProviderFlagType
{
    const NAME        = 'ProviderFlagType';
    const NAME_SPACE  = AppBundle::ENTITY_NAMESPACE.self::NAME;
    const ENTITY_NAME = AppBundle::NAME.':'.self::NAME;

    const CERTIFIED          = 'Certified';
    const ACCEPTING_PATIENTS = 'Accepting Patients';


    use TypeTrait;
}
