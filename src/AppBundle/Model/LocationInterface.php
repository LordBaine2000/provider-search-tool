<?php

namespace AppBundle\Model;

use AppBundle\Entity\LocationType;

/**
 * Interface for Entities that have relationships to LocationType
 */
interface LocationInterface
{
    /**
     * Get location
     *
     * @return LocationType
     */
    public function getLocation();

    /**
     * Set location
     *
     * @param LocationType $location
     * @return $this
     */
    public function setLocation(LocationType $location);
}
