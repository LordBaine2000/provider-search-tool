<?php

namespace AppBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

/**
 * Interface for Provider repositories
 */
interface ProviderRepositoryInterface
{
    /**
     * Find Provider by ProviderSearchFormType
     *
     * @param Form $form
     * @return ArrayCollection<Provider>
     */
    public function findBySearchForm(Form $form);
}
