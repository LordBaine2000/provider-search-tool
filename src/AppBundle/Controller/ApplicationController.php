<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\AncillaryType;
use AppBundle\Entity\FacilityType;
use AppBundle\Entity\PlanType;
use AppBundle\Entity\Provider;
use AppBundle\Entity\ProviderType;
use AppBundle\Entity\SpecialtyType;
use AppBundle\Form\FormType\DynamicEntitySelectionFormType;
use AppBundle\Form\FormType\ProviderSearchFormType;
use AppBundle\Form\FormType\SuggestProviderFormType;
use AppBundle\Repository\FacilityTypeRepository;
use AppBundle\Repository\ProviderTypeRepository;
use AppBundle\Service\ProviderRouteFactory;
use AppBundle\Service\ProviderSearchService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controls application view rendering and form processing
 */
class ApplicationController extends Controller
{
    const NAME                     = 'ApplicationController';
    const NAME_SPACE               = AppBundle::CONTROLLER_NAMESPACE.self::NAME;

    const PLAN_TYPE_ROUTE          = 'plan_type';
    const PROVIDER_TYPE_ROUTE      = 'provider_type';
    const SPECIALTY_TYPE_ROUTE     = 'specialty_type';
    const FACILITY_TYPE_ROUTE      = 'facility_type';
    const ANCILLARY_TYPE_ROUTE     = 'ancillary_type';
    const LOCATION_ROUTE           = 'location';
    const SPECIALTY_LOCATION_ROUTE = 'specialty_location';
    const FACILITY_LOCATION_ROUTE  = 'facility_location';
    const ANCILLARY_LOCATION_ROUTE = 'ancillary_location';
    const DENTAL_ROUTE             = 'dental';
    const SUGGEST_PROVIDER_ROUTE   = 'suggest_provider';
    const HANDLE_RESULTS_ROUTE     = 'handle_results';
    const SUBMIT_RESULTS_ROUTE     = 'submit_results';
    const PROVIDER_DETAILS_ROUTE   = 'provider_details';


    /**
     * PlanType selection
     *
     * @Route("/", name=ApplicationController::PLAN_TYPE_ROUTE)
     * @Template()
     *
     * @param  Request $request
     * @return array
     */
    public function planTypeAction(Request $request)
    {
        /** @var Form $form */
        $form = $this->createForm(DynamicEntitySelectionFormType::NAME, null, [
            DynamicEntitySelectionFormType::CLASS_OPTION => PlanType::NAME,
            'action' => $this->generateUrl($request->get('_route')),
            'method' => Request::METHOD_POST
        ]);

        $form->handleRequest($request);

        return ($form->isSubmitted() && $form->isValid()) ?
            $this->redirectToRoute(self::PROVIDER_TYPE_ROUTE, [
                'slug' => $form->get(PlanType::NAME)->getData()->getSlug()
            ]) :
            ['form' => $form->createView()];
    }

    /**
     * ProviderType selection
     *
     * @Route("/plan/{slug}/provider", name=ApplicationController::PROVIDER_TYPE_ROUTE)
     * @Template()
     *
     * @param  Request  $request
     * @param  PlanType $planType
     * @return array
     */
    public function providerTypeAction(Request $request, PlanType $planType)
    {
        /** @var Form $form */
        $form = $this->createForm(DynamicEntitySelectionFormType::NAME, null, [
            DynamicEntitySelectionFormType::CLASS_OPTION => ProviderType::NAME,
            'action' => $this->generateUrl($request->get('_route'), [
                'slug' => $planType->getSlug()
            ]),
            'method' => Request::METHOD_POST,
            'query_builder' => function (ProviderTypeRepository $repository) {
                return $repository->getMetaTypesQB();
            }
        ]);

        $form->handleRequest($request);

        /** @var ProviderType $providerType */
        return ($form->isSubmitted() && $form->isValid() && $providerType = $form->get(ProviderType::NAME)->getData()) ?
            $this->redirectToRoute(
                $this->get(ProviderRouteFactory::SERVICE_NAME)->get($providerType),
                [
                    'slug'             => $planType->getSlug(),
                    'providerTypeSlug' => $providerType->getSlug()
                ]) :
            ['form' => $form->createView()];
    }

    /**
     * SpecialtyType selection
     *
     * @Route("/plan/{slug}/provider/{providerTypeSlug}/specialty", name=ApplicationController::SPECIALTY_TYPE_ROUTE)
     * @ParamConverter("providerType", class=ProviderType::ENTITY_NAME, options={"mapping": {"providerTypeSlug": "slug"}})
     * @Template()
     *
     * @param  Request      $request
     * @param  PlanType     $planType
     * @param  ProviderType $providerType
     * @return array
     */
    public function specialtyTypeAction(Request $request, PlanType $planType, ProviderType $providerType)
    {
        /** @var Form $form */
        $form = $this->createForm(DynamicEntitySelectionFormType::NAME, null, [
            DynamicEntitySelectionFormType::CLASS_OPTION => SpecialtyType::NAME,
            'action' => $this->generateUrl($request->get('_route'), [
                'slug'             => $planType->getSlug(),
                'providerTypeSlug' => $providerType->getSlug()
            ]),
            'method' => Request::METHOD_POST
        ]);

        $form->handleRequest($request);

        /** @var SpecialtyType $specialtyType */
        return ($form->isSubmitted() && $form->isValid() && $specialtyType = $form->get(SpecialtyType::NAME)->getData()) ?
            $this->redirectToRoute(
                self::SPECIALTY_LOCATION_ROUTE,
                [
                    'slug'              => $planType->getSlug(),
                    'providerTypeSlug'  => $providerType->getSlug(),
                    'specialtyTypeSlug' => $specialtyType->getSlug()
                ]) :
            ['form' => $form->createView()];
    }

    /**
     * FacilityType selection
     *
     * @Route("/plan/{slug}/provider/{providerTypeSlug}/facility", name=ApplicationController::FACILITY_TYPE_ROUTE)
     * @ParamConverter("providerType", class=ProviderType::ENTITY_NAME, options={"mapping": {"providerTypeSlug": "slug"}})
     * @Template()
     *
     * @param  Request      $request
     * @param  PlanType     $planType
     * @param  ProviderType $providerType
     * @return array
     */
    public function facilityTypeAction(Request $request, PlanType $planType, ProviderType $providerType)
    {
        /** @var Form $form */
        $form = $this->createForm(DynamicEntitySelectionFormType::NAME, null, [
            DynamicEntitySelectionFormType::CLASS_OPTION => FacilityType::NAME,
            'action' => $this->generateUrl($request->get('_route'), [
                'slug' => $planType->getSlug(),
                'providerTypeSlug' => $providerType->getSlug()
            ]),
            'method' => Request::METHOD_POST,
            'query_builder' => function (FacilityTypeRepository $repository) {
                return $repository->getSubtypesQB();
            }
        ]);

        $form->handleRequest($request);

        /** @var FacilityType $facilityType */
        return ($form->isSubmitted() && $form->isValid() && $facilityType = $form->get(FacilityType::NAME)->getData()) ?
            $this->redirectToRoute(
                self::FACILITY_LOCATION_ROUTE,
                [
                    'slug'             => $planType->getSlug(),
                    'providerTypeSlug' => $providerType->getSlug(),
                    'facilityTypeSlug' => $facilityType->getSlug()
                ]) :
            ['form' => $form->createView()];
    }

    /**
     * AncillaryType selection
     *
     * @Route("/plan/{slug}/provider/{providerTypeSlug}/type", name=ApplicationController::ANCILLARY_TYPE_ROUTE)
     * @ParamConverter("providerType", class=ProviderType::ENTITY_NAME, options={"mapping": {"providerTypeSlug": "slug"}})
     * @Template()
     *
     * @param  Request      $request
     * @param  PlanType     $planType
     * @param  ProviderType $providerType
     * @return array
     */
    public function ancillaryTypeAction(Request $request, PlanType $planType, ProviderType $providerType)
    {
        /** @var Form $form */
        $form = $this->createForm(DynamicEntitySelectionFormType::NAME, null, [
            DynamicEntitySelectionFormType::CLASS_OPTION => AncillaryType::NAME,
            'action' => $this->generateUrl($request->get('_route'), [
                'slug' => $planType->getSlug(),
                'providerTypeSlug' => $providerType->getSlug()
            ]),
            'method' => Request::METHOD_POST
        ]);

        $form->handleRequest($request);

        /** @var AncillaryType $ancillaryType */
        return ($form->isSubmitted() && $form->isValid() && $ancillaryType = $form->get(AncillaryType::NAME)->getData()) ?
            $this->redirectToRoute(
                self::ANCILLARY_LOCATION_ROUTE,
                [
                    'slug'              => $planType->getSlug(),
                    'providerTypeSlug'  => $providerType->getSlug(),
                    'ancillaryTypeSlug' => $ancillaryType->getSlug()
                ]) :
            ['form' => $form->createView()];
    }

    /**
     * Location information
     *
     * @Route("/plan/{slug}/provider/{providerTypeSlug}/location",
     *     name=ApplicationController::LOCATION_ROUTE)
     * @Route("/plan/{slug}/provider/{providerTypeSlug}/specialty/{specialtyTypeSlug}/location",
     *     name=ApplicationController::SPECIALTY_LOCATION_ROUTE)
     * @Route("/plan/{slug}/provider/{providerTypeSlug}/facility/{facilityTypeSlug}/location",
     *     name=ApplicationController::FACILITY_LOCATION_ROUTE)
     * @Route("/plan/{slug}/provider/{providerTypeSlug}/type/{ancillaryTypeSlug}/location",
     *     name=ApplicationController::ANCILLARY_LOCATION_ROUTE)
     *
     * @ParamConverter("providerType",  class=ProviderType::ENTITY_NAME,  options={"mapping": {"providerTypeSlug": "slug"}})
     * @ParamConverter("specialtyType", class=SpecialtyType::ENTITY_NAME, options={"mapping": {"specialtyTypeSlug": "slug"}})
     * @ParamConverter("facilityType",  class=FacilityType::ENTITY_NAME,  options={"mapping": {"facilityTypeSlug": "slug"}})
     * @ParamConverter("ancillaryType", class=AncillaryType::ENTITY_NAME, options={"mapping": {"ancillaryTypeSlug": "slug"}})
     *
     * @Template()
     *
     * @param  PlanType      $planType
     * @param  ProviderType  $providerType
     * @param  SpecialtyType $specialtyType
     * @param  FacilityType  $facilityType
     * @param  AncillaryType $ancillaryType
     * @return array
     */
    public function locationAction
    (
        PlanType $planType,
        ProviderType $providerType,
        SpecialtyType $specialtyType = null,
        FacilityType $facilityType = null,
        AncillaryType $ancillaryType = null
    ) {
        /** @var Form $form */
        $form = $this->createForm(ProviderSearchFormType::NAME, [
            PlanType::NAME      => $planType,
            ProviderType::NAME  => $providerType,
            SpecialtyType::NAME => $specialtyType,
            FacilityType::NAME  => $facilityType,
            AncillaryType::NAME => $ancillaryType,
        ], [
            ProviderSearchFormType::HIDE_FIELDS_OPTION => true,
            'action' => $this->generateUrl(self::SUBMIT_RESULTS_ROUTE),
            'method' => Request::METHOD_POST
        ]);

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * Dental lightbox
     *
     * @Route("/plan/{slug}/provider/{providerTypeSlug}", name=ApplicationController::DENTAL_ROUTE)
     * @ParamConverter("providerType",  class=ProviderType::ENTITY_NAME,  options={"mapping": {"providerTypeSlug": "slug"}})
     *
     * @param  PlanType $planType
     * @return array
     */
    public function dentalAction(PlanType $planType)
    {
        return ($planType->getSlug() == PlanType::EPO_SLUG || $planType->getSlug() == PlanType::PPO_SLUG) ?
            $this->render('@App/Application/dentalDelta.html.twig') :
            $this->render('@App/Application/dentalHealthplex.html.twig');
    }

    /**
     * Suggest a Provider
     *
     * NOTE: If this gets any more complex, or any other email functionality is requested this should be refactored to
     *       use an EmailService
     *
     * @Route("/suggest-provider", name=ApplicationController::SUGGEST_PROVIDER_ROUTE)
     * @Template()
     *
     * @param  Request $request
     * @return array
     */
    public function suggestProviderAction(Request $request)
    {
        $form = $this->createForm(SuggestProviderFormType::NAME, null, [
            'action' => $this->generateUrl($request->get('_route')),
            'method' => Request::METHOD_POST
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $emailParams = $this->getParameter('email.suggest_provider');

            $message = \Swift_Message::newInstance();
            $message
                ->setSubject($emailParams['subject'])
                ->setFrom($emailParams['from_address'])
                ->setTo($emailParams['to_address'])
                ->setBody(
                    //TODO: Format email template
                    $this->renderView('AppBundle:Emails:suggestProvider.html.twig', ['data' => $form->getData()]),
                    'text/html'
                )
            ;

            if($this->get('mailer')->send($message))
            {
                $this->addFlash('notice', 'label.mailer.success');
            } else {
                $this->addFlash('error', 'label.mailer.failure');
            }
        }

        return ['form' => $form->createView()];
    }

    /**
     * Provider results
     *
     * @Route("/provider-results/handle", name=ApplicationController::HANDLE_RESULTS_ROUTE)
     * @Route("/provider-results/submit", name=ApplicationController::SUBMIT_RESULTS_ROUTE)
     * @Template()
     *
     * @param  Request $request
     * @return array
     */
    public function resultsAction(Request $request)
    {
        $form = $this->createForm(ProviderSearchFormType::NAME, null, [
            'action' => $this->generateUrl(self::HANDLE_RESULTS_ROUTE),
            'method' => Request::METHOD_POST
        ]);

        if($request->get('_route') == self::SUBMIT_RESULTS_ROUTE && $request->getMethod() != Request::METHOD_GET)
        {
            //handleRequest will blow away child form default values
            $form->submit($request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        return [
            'form' => $form->createView(),
            'results' => $this->get(ProviderSearchService::SERVICE_NAME)
                ->search($form)
        ];
    }

    /**
     * Provider details lightbox
     *
     * @Route("/provider/{id}", name=ApplicationController::PROVIDER_DETAILS_ROUTE)
     * @Template()
     *
     * @param  Provider $provider
     * @return array
     */
    public function providerDetailsAction(Provider $provider)
    {
        return [
            'provider' => $provider
        ];
    }
}
