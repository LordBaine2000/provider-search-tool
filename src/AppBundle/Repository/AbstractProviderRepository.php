<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use AppBundle\Entity\Address;
use AppBundle\Entity\CoreType;
use AppBundle\Entity\LocationType;
use AppBundle\Entity\PlanType;
use AppBundle\Entity\Postcode;
use AppBundle\Entity\Provider;
use AppBundle\Form\FormType\LocationFormType;
use AppBundle\Form\FormType\ProviderFiltersFormType;
use AppBundle\Form\FormType\ProviderSearchFormType;
use AppBundle\Model\ProviderRepositoryInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

/**
 * Abstract repository for Provider queries
 */
abstract class AbstractProviderRepository extends BaseRepository implements ProviderRepositoryInterface
{
    const NAME       = 'AbstractProviderRepository';
    const NAME_SPACE = AppBundle::REPOSITORY_NAMESPACE.self::NAME;

    const MILES           = 3959;
    const KILOMETERS      = 6371;
    const DISTANCE_RESULT = 'Distance';


    /**
     * Base query for findBySearchForm methods
     *
     * @param Form $form ProviderSearchForm
     * @param string $alias Table alias
     * @param bool $kilometers Search by Kilometers instead of Miles
     * @return QueryBuilder
     * @throws \LogicException
     */
    protected function getBaseQuery(Form $form, $alias, $kilometers = false)
    {
        $locationForm = $form->get(LocationFormType::NAME);
        $postcode = $locationForm->get(LocationFormType::POSTCODE_FIELD)->getData();

        if(!($postcode instanceof Postcode) || !($location = $postcode->getLocation()))
        {
            throw new \LogicException(sprintf(
                'AbstractProviderRepository expected an instance of %s to have an associated %s, but did not find one',
                    Postcode::NAME,
                    LocationType::NAME
            ));
        }

        $distanceConst = ($kilometers) ? self::KILOMETERS : self::MILES;
        $locationAlias = LocationType::NAME;

        // See: https://developers.google.com/maps/articles/phpsqlsearch_v3
        $haversineFormula = "( $distanceConst * ACOS(COS(RADIANS(:latitude)) * COS(RADIANS(Y($locationAlias.point)))
            * COS(RADIANS(X($locationAlias.point)) - RADIANS(:longitude))
            + SIN(RADIANS(:latitude)) * SIN(RADIANS(Y($locationAlias.point)))) )";

        $qb = $this->createQueryBuilder($alias)
            ->select("$alias AS ".Provider::NAME)
            ->addSelect("$haversineFormula AS ".self::DISTANCE_RESULT)
            ->innerJoin("$alias.acceptedPlans", PlanType::NAME, Expr\Join::WITH, PlanType::NAME.' = :planType')
            ->innerJoin("$alias.addresses", Address::NAME)
            ->innerJoin(Address::NAME.'.location', $locationAlias)
            ->having(self::DISTANCE_RESULT.' <= :radius')
            ->setParameters([
                'planType'      => $form->get(ProviderSearchFormType::PLAN_TYPE_FIELD)->getData(),
                'latitude'      => $location->getPoint()->getLatitude(),
                'longitude'     => $location->getPoint()->getLongitude(),
                'radius'        => $locationForm->get(LocationFormType::RADIUS_FIELD)->getData()
            ]);

        $this->addProviderFilters($qb, $form->get(ProviderFiltersFormType::NAME), $alias);

        return $qb;
    }

    /**
     * Adds additional business logic to base query
     *
     * @param QueryBuilder $qb
     * @param FormInterface $form
     * @param $alias
     */
    private function addProviderFilters(QueryBuilder $qb, FormInterface $form, $alias)
    {
        if($form->get(ProviderFiltersFormType::PREFERRED_ONLY_FIELD)->getData())
        {
            $qb
                ->innerJoin("$alias.coreType", CoreType::NAME, Expr\Join::WITH, CoreType::NAME.'.name = :coreType')
                ->setParameter('coreType', CoreType::A)
            ;
        } else {
            $qb->leftJoin("$alias.coreType", CoreType::NAME);
        }

        if($form->get(ProviderFiltersFormType::SORT_BY_FIELD)->getData() == ProviderFiltersFormType::RANK_SORT_OPTION)
        {
            $qb
                ->addSelect('-'.CoreType::NAME.'.name AS HIDDEN coreType')
                ->orderBy('coreType', 'DESC')
                ->addOrderBy(CoreType::NAME.'.name', 'ASC')
                ->addOrderBy(self::DISTANCE_RESULT, 'ASC')
            ;
        } else {
            $qb->orderBy(self::DISTANCE_RESULT, 'ASC');
        }

        $network = $form->get(ProviderFiltersFormType::NETWORK_FIELD)->getData();

        if(!in_array(ProviderFiltersFormType::CORE_NETWORK_OPTION, $network))
        {
            $qb->andWhere($qb->expr()->isNull(CoreType::NAME));
        }

        if(!in_array(ProviderFiltersFormType::EXTENDED_NETWORK_OPTION, $network))
        {
            $qb->andWhere($qb->expr()->isNotNull(CoreType::NAME));
        }
    }
}
