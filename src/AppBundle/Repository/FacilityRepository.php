<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use AppBundle\Form\FormType\ProviderSearchFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

/**
 * Repository for custom Facility queries
 */
class FacilityRepository extends AbstractProviderRepository
{
    const NAME         = 'FacilityRepository';
    const NAME_SPACE   = AppBundle::REPOSITORY_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.repository.facility';


    /**
     * Find Facility by Form
     *
     * @param Form $form
     * @return ArrayCollection<Facility>
     */
    public function findBySearchForm(Form $form)
    {
        return $this->getBaseQuery($form, 'f')
            ->andWhere('f.type = :facilityType')
            ->setParameter('facilityType', $form->get(ProviderSearchFormType::FACILITY_TYPE_FIELD)->getData())
            ->getQuery()
            ->getResult();
    }
}
