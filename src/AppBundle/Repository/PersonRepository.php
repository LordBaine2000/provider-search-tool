<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use AppBundle\Entity\GenderType;
use AppBundle\Entity\LanguageType;
use AppBundle\Entity\ProviderFlagType;
use AppBundle\Form\FormType\PersonFiltersFormType;
use AppBundle\Form\FormType\ProviderSearchFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;
use Doctrine\ORM\Query\Expr;

/**
 * Repository for custom Person queries
 */
class PersonRepository extends AbstractProviderRepository
{
    const NAME         = 'PersonRepository';
    const NAME_SPACE   = AppBundle::REPOSITORY_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.repository.person';


    /**
     * Find Person by ProviderSearchFormType
     *
     * @param Form $form
     * @return ArrayCollection<Person>
     */
    public function findBySearchForm(Form $form)
    {
        $qb = $this->getBaseQuery($form, 'p');

        $qb
            ->innerJoin("p.gender", GenderType::NAME, Expr\Join::WITH, $qb->expr()->in(GenderType::NAME, ':genderTypes'))
            ->innerJoin("p.languages", LanguageType::NAME, Expr\Join::WITH, $qb->expr()->in(LanguageType::NAME, ':languageTypes'))
            ->leftJoin("p.flags", ProviderFlagType::CERTIFIED, Expr\Join::WITH, ProviderFlagType::CERTIFIED.'.name = :certifiedFlagType')
        ;

        $personFilterForm = $form->get(PersonFiltersFormType::NAME);
        $certification = $personFilterForm->get(PersonFiltersFormType::CERTIFICATION_FIELD)->getData();

        if(!in_array(PersonFiltersFormType::CERTIFIED_CERTIFICATION_OPTION, $certification))
        {
            $qb->andWhere($qb->expr()->isNull(ProviderFlagType::CERTIFIED));
        }

        if(!in_array(PersonFiltersFormType::UNCERTIFIED_CERTIFICATION_OPTION, $certification))
        {
            $qb->andWhere($qb->expr()->isNotNull(ProviderFlagType::CERTIFIED));
        }

        $qb
            ->setParameter('genderTypes', $personFilterForm->get(PersonFiltersFormType::GENDER_FIELD)->getData()->toArray())
            ->setParameter('languageTypes', $personFilterForm->get(PersonFiltersFormType::LANGUAGE_FIELD)->getData()->toArray())
            ->setParameter('certifiedFlagType', ProviderFlagType::CERTIFIED)
            ->innerJoin('p.specialties', 'st', Expr\Join::WITH, 'st = :specialtyType')
            ->setParameter('specialtyType', $form->get(ProviderSearchFormType::SPECIALTY_TYPE_FIELD)->getData())
        ;

        return $qb
            ->getQuery()
            ->getResult();
    }
}
