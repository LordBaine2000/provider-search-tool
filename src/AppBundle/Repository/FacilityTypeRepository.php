<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use Doctrine\ORM\QueryBuilder;

/**
 * Repository for custom FacilityType queries
 */
class FacilityTypeRepository extends BaseRepository
{
    const NAME         = 'FacilityTypeRepository';
    const NAME_SPACE   = AppBundle::REPOSITORY_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.repository.facility_type';


    /**
     * Gets specific types based on business logic
     *
     * @return QueryBuilder
     */
    public function getSubtypesQB()
    {
        return $this->createQueryBuilder('ft')
            ->where('ft INSTANCE OF AppBundle:FacilitySubtype');
    }
}
