<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use AppBundle\Form\FormType\ProviderSearchFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

/**
 * Repository for custom Ancillary queries
 */
class AncillaryRepository extends AbstractProviderRepository
{
    const NAME         = 'AncillaryRepository';
    const NAME_SPACE   = AppBundle::REPOSITORY_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.repository.ancillary';


    /**
     * Find Ancillary by Form
     *
     * @param Form $form
     * @return ArrayCollection<Ancillary>
     */
    public function findBySearchForm(Form $form)
    {
        return $this->getBaseQuery($form, 'a')
            ->andWhere('a.type = :ancillaryType')
            ->setParameter('ancillaryType', $form->get(ProviderSearchFormType::ANCILLARY_TYPE_FIELD)->getData())
            ->getQuery()
            ->getResult();
    }
}
