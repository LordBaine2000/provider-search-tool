<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\RecursiveValidator;

/**
 * Base application repository
 */
class BaseRepository extends EntityRepository
{
    const NAME         = 'BaseRepository';
    const NAME_SPACE   = AppBundle::REPOSITORY_NAMESPACE.self::NAME;

    const LOCATION_TYPE_REPOSITORY_SERVICE_NAME = 'app.repository.location_type';
    const POSTCODE_REPOSITORY_SERVICE_NAME      = 'app.repository.postcode';
    const ADDRESS_REPOSITORY_SERVICE_NAME       = 'app.repository.address';
    const GENDER_TYPE_REPOSITORY_SERVICE_NAME   = 'app.repository.gender_type';
    const LANGUAGE_TYPE_REPOSITORY_SERVICE_NAME = 'app.repository.language_type';


    /** @var RecursiveValidator */
    private $validator;


    /**
     * @see ObjectManager::persist
     */
    public function persist($object)
    {
        $errors = $this->validator->validate($object);

        if(count($errors))
        {
            throw new ValidatorException((string) $errors);
        } else {
            $this->_em->persist($object);
        }
    }

    /**
     * @see ObjectManager::remove
     */
    public function remove($object)
    {
        $this->_em->remove($object);
    }

    /**
     * @see ObjectManager::merge
     */
    public function merge($object)
    {
        $this->_em->merge($object);
    }

    /**
     * @see ObjectManager::detach
     */
    public function detach($object)
    {
        $this->_em->detach($object);
    }

    /**
     * @see ObjectManager::refresh
     */
    public function refresh($object)
    {
        $this->_em->refresh($object);
    }

    /**
     * @see ObjectManager::flush
     */
    public function flush()
    {
        $this->_em->flush();
    }

    /**
     * Set validator
     *
     * @param RecursiveValidator $validator
     * @return $this
     */
    public function setValidator(RecursiveValidator $validator)
    {
        $this->validator = $validator;

        return $this;
    }
}
