<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use AppBundle\Entity\FacilityType;
use Doctrine\ORM\QueryBuilder;

/**
 * Repository for custom ProviderType queries
 */
class ProviderTypeRepository extends BaseRepository
{
    const NAME         = 'ProviderTypeRepository';
    const NAME_SPACE   = AppBundle::REPOSITORY_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.repository.provider_type';


    /**
     * Filters dental out of MetaTypes
     *
     * @return QueryBuilder
     */
    public function filterDentalQB()
    {
        return $this->getMetaTypesQB()
                ->andWhere('pt.slug != :dental_slug')
                ->setParameter('dental_slug', FacilityType::DENTAL_SLUG)
            ;
    }

    /**
     * Gets specific types based on business logic
     *
     * @return QueryBuilder
     */
    public function getMetaTypesQB()
    {
        return $this->createQueryBuilder('pt')
            ->where('pt INSTANCE OF AppBundle:ProviderType OR pt INSTANCE OF AppBundle:FacilityType');
    }
}
