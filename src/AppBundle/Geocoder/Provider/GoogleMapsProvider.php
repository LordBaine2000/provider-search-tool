<?php

namespace AppBundle\Geocoder\Provider;

use AppBundle\AppBundle;
use Geocoder\HttpAdapter\HttpAdapterInterface;
use Geocoder\Provider\GoogleMapsProvider as Base;

/**
 * Override for component filters
 *
 * @see https://github.com/geocoder-php/Geocoder/pull/427
 */
class GoogleMapsProvider extends Base
{
    const NAME         = 'GoogleMapsProvider';
    const NAME_SPACE   = AppBundle::GEOCODER_PROVIDER_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'bazinga_geocoder.provider.google_maps';


    /**
     * @var string
     */
    private $apiKey = null;

    /**
     * @var array
     */
    private $componentFilters;

    /**
     * @inheritdoc
     */
    public function __construct(HttpAdapterInterface $adapter, $locale = null, $region = null, $useSsl = false, $apiKey = null)
    {
        parent::__construct($adapter, $locale, $region, $useSsl, $apiKey);

        $this->apiKey = $apiKey;
    }

    /**
     * Add components for filtering.
     * https://developers.google.com/maps/documentation/geocoding/#ComponentFiltering
     *
     * @param array $filters
     *
     * @return $this
     */
    public function setComponentFilters(array $filters)
    {
        $this->componentFilters = $filters;

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function buildQuery($query)
    {
        if (null !== $this->getLocale()) {
            $query = sprintf('%s&language=%s', $query, $this->getLocale());
        }

        if (null !== $this->getRegion()) {
            $query = sprintf('%s&region=%s', $query, $this->getRegion());
        }

        if (null !== $this->apiKey) {
            $query = sprintf('%s&key=%s', $query, $this->apiKey);
        }

        if (!empty($this->componentFilters)) {
            $componentFilters = $this->componentFilters;
            $query = sprintf('%s&components=%s', $query, implode('|', array_map(function($key) use ($componentFilters) {
                return $key.':'.urlencode($componentFilters[$key]);
            }, array_keys($componentFilters))));
        }

        return $query;
    }
}
