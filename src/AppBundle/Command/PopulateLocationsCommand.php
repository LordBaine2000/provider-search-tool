<?php

namespace AppBundle\Command;

use AppBundle\AppBundle;
use AppBundle\Repository\BaseRepository;
use AppBundle\Service\LocationService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Symfony console command to generate LocationTypes for LocationInterface entities
 */
class PopulateLocationsCommand extends ContainerAwareCommand
{
    const NAME         = 'PopulateLocationsCommand';
    const NAME_SPACE   = AppBundle::COMMAND_NAMESPACE.self::NAME;

    const COMMAND_NAME = 'application:locations:validate';


    /**
     * Command configuration
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription(
                'Generate LocationTypes for all LocationInterface entities that don\'t already have them via Google Geocoding API '
            )
        ;
    }

    /**
     * Generate LocationType objects for all Addresses in the database
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int 0 if everything went fine, or an error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write(
            'Validating all LocationInterface entities via Google Geocoding API.'
        );

        foreach($this->getContainer()->get(BaseRepository::ADDRESS_REPOSITORY_SERVICE_NAME)->findAll() as $address)
        {
            $this->getContainer()->get(LocationService::SERVICE_NAME)->locateAddress($address);
            $output->write('.');
        }

        foreach($this->getContainer()->get(BaseRepository::POSTCODE_REPOSITORY_SERVICE_NAME)->findAll() as $postcode)
        {
            $this->getContainer()->get(LocationService::SERVICE_NAME)->locatePostcode($postcode);
            $output->write('.');
        }

        $output->writeln(
            "\nLocationTypes have been successfully validated."
        );

        return 0;
    }
}
