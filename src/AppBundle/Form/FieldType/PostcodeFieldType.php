<?php

namespace AppBundle\Form\FieldType;

use AppBundle\AppBundle;
use AppBundle\Entity\Postcode;
use AppBundle\Form\DataTransformer\EntityTransformer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Transforms a numeric postcode into a Postcode entity
 */
class PostcodeFieldType extends AbstractType
{
    const NAME         = 'PostcodeFieldType';
    const NAME_SPACE   = AppBundle::FORM_FIELD_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.field.type.postcode';


    /** @var EntityManager */
    private $em;


    /**
     * Constructor
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Configure field type
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new EntityTransformer($this->em, Postcode::ENTITY_NAME, 'name'));
    }

    /**
     * Configure form options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'invalid_message' => 'The entity does not exist.',
            ])
        ;
    }

    /**
     * Get parent field type
     *
     * @return string
     */
    public function getParent()
    {
        return 'integer';
    }

    /**
     * Get field name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
