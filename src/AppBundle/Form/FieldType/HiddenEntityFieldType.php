<?php

namespace AppBundle\Form\FieldType;

use AppBundle\AppBundle;
use AppBundle\Form\DataTransformer\EntityTransformer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Hidden field type for entities
 */
class HiddenEntityFieldType extends AbstractType
{
    const NAME         = 'HiddenEntityFieldType';
    const NAME_SPACE   = AppBundle::FORM_FIELD_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.field.type.hidden_entity';

    const CLASS_OPTION = 'class';


    /** @var EntityManager */
    private $em;


    /**
     * Constructor
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Configure field type
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new EntityTransformer($this->em, $options[self::CLASS_OPTION], 'id'));
    }

    /**
     * Configure form options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired([self::CLASS_OPTION])
            ->setDefaults([
                'invalid_message' => 'The entity does not exist.'
            ])
        ;
    }

    /**
     * Get parent field type
     *
     * @return string
     */
    public function getParent()
    {
        return 'hidden';
    }

    /**
     * Get field name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
