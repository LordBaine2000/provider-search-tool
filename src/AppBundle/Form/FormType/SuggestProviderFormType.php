<?php

namespace AppBundle\Form\FormType;

use AppBundle\AppBundle;
use AppBundle\Entity\AncillaryType;
use AppBundle\Entity\FacilityType;
use AppBundle\Entity\Phone;
use AppBundle\Entity\PlanType;
use AppBundle\Entity\Provider;
use AppBundle\Entity\ProviderType;
use AppBundle\Entity\SpecialtyType;
use AppBundle\Form\FieldType\HiddenEntityFieldType;
use AppBundle\Repository\FacilityTypeRepository;
use AppBundle\Repository\ProviderTypeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Provider suggestion form
 */
class SuggestProviderFormType extends AbstractType
{
    const NAME         = 'SuggestProviderFormType';
    const NAME_SPACE   = AppBundle::FORM_FORM_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.form.type.suggest_provider';

    const PROVIDER_TYPE_FIELD  = ProviderType::NAME;
    const PLAN_TYPE_FIELD      = PlanType::NAME;
    const PROVIDER_FIELD       = Provider::NAME;
    const PHONE_FIELD          = Phone::NAME;
    const SPECIALTY_TYPE_FIELD = SpecialtyType::NAME;
    const FACILITY_TYPE_FIELD  = FacilityType::NAME;
    const ANCILLARY_TYPE_FIELD = AncillaryType::NAME;


    /** @var ProviderTypeRepository */
    private $providerTypeRepository;


    /**
     * Constructor
     *
     * @param ProviderTypeRepository $providerTypeRepository
     */
    public function __construct(ProviderTypeRepository $providerTypeRepository)
    {
        $this->providerTypeRepository = $providerTypeRepository;
    }

    /**
     * Configure form fields
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(self::PROVIDER_TYPE_FIELD, 'entity', [
                'class' => ProviderType::NAME_SPACE,
                'query_builder' => function (ProviderTypeRepository $repository) {
                    return $repository->getMetaTypesQB();
                }
            ])
            ->add(self::PLAN_TYPE_FIELD, 'entity', [
                'class' => PlanType::ENTITY_NAME,
                'choice_label'   => 'name',
                'multiple'       => true,
                'error_bubbling' => true,
                'constraints'    => new Count([
                    'min'        => 1,
                    'minMessage' => 'label.error.plan_type_blank'
                ])
            ])
            ->add(self::PROVIDER_FIELD, 'text', [
                'error_bubbling' => true,
                'constraints'    => new NotBlank([
                    'message' => 'label.error.provider_blank'
                ])
            ])
            ->add(AddressFormType::NAME, new AddressFormType())
            ->add(self::PHONE_FIELD, 'integer', [
                'error_bubbling' => true,
                'constraints'    => new NotBlank([
                    'message' => 'label.error.phone_blank'
                ])
            ])
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                [$this, 'addFields']
            )
        ;
    }

    /**
     * Add relevant subtype fields in PRE_SUBMIT so they can be bound
     *
     * @param FormEvent $event
     */
    public function addFields(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $providerType = $this->providerTypeRepository->find($data[self::PROVIDER_TYPE_FIELD]);

        switch($providerType->getSlug())
        {
            case ProviderType::PRIMARY_SLUG:
                $form->add(self::SPECIALTY_TYPE_FIELD, 'entity', [
                    'class' => SpecialtyType::ENTITY_NAME,
                    'choice_label' => 'name'
                ]);
                break;
            case ProviderType::HOSPITAL_SLUG:
                $form->add(self::FACILITY_TYPE_FIELD, 'entity', [
                    'class' => FacilityType::ENTITY_NAME,
                    'choice_label' => 'name',
                    'query_builder' => function (FacilityTypeRepository $repository) {
                        return $repository->getSubtypesQB();
                    }
                ]);
                break;
            case ProviderType::ANCILLARY_SLUG:
                $form->add(self::ANCILLARY_TYPE_FIELD, 'entity', [
                    'class' => AncillaryType::ENTITY_NAME,
                    'choice_label' => 'name'
                ]);
                break;
            default:
                $form->add(self::FACILITY_TYPE_FIELD, HiddenEntityFieldType::NAME, [
                    HiddenEntityFieldType::CLASS_OPTION => FacilityType::ENTITY_NAME
                ]);

                $data[self::FACILITY_TYPE_FIELD] = $data[self::PROVIDER_TYPE_FIELD];

                $event->setData($data);
        }
    }

    /**
     * Get form name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
