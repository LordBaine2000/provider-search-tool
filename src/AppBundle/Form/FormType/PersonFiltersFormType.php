<?php

namespace AppBundle\Form\FormType;

use AppBundle\AppBundle;
use AppBundle\Entity\GenderType;
use AppBundle\Entity\LanguageType;
use AppBundle\Repository\BaseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Person filters form
 */
class PersonFiltersFormType extends AbstractType
{
    const NAME         = 'PersonFiltersFormType';
    const NAME_SPACE   = AppBundle::FORM_FORM_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.form.type.person_filters';

    const GENDER_FIELD                     = 'gender';
    const LANGUAGE_FIELD                   = 'language';
    const CERTIFICATION_FIELD              = 'certification';
    const CERTIFIED_CERTIFICATION_OPTION   = 'certified';
    const UNCERTIFIED_CERTIFICATION_OPTION = 'uncertified';


    /** @var BaseRepository */
    private $genderTypeRepository;

    /** @var BaseRepository */
    private $languageTypeRepository;


    /**
     * Constructor
     *
     * @param BaseRepository $genderTypeRepository
     * @param BaseRepository $languageTypeRepository
     */
    public function __construct(BaseRepository $genderTypeRepository, BaseRepository $languageTypeRepository)
    {
        $this->genderTypeRepository   = $genderTypeRepository;
        $this->languageTypeRepository = $languageTypeRepository;
    }


    /**
     * Configure form fields
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(self::GENDER_FIELD, 'entity', [
                'class'        => GenderType::NAME_SPACE,
                'choice_label' => 'name',
                'expanded'     => true,
                'multiple'     => true,
                'data'         => new ArrayCollection($this->genderTypeRepository->findAll())
            ])
            ->add(self::LANGUAGE_FIELD, 'entity', [
                'class'        => LanguageType::NAME_SPACE,
                'choice_label' => 'name',
                'multiple'     => true,
                'required'     => false,
                'data'         => new ArrayCollection($this->languageTypeRepository->findAll())
            ])
            ->add(self::CERTIFICATION_FIELD, 'choice', [
                'expanded' => true,
                'multiple' => true,
                'choices'  => [
                    self::CERTIFIED_CERTIFICATION_OPTION   => 'label.certified',
                    self::UNCERTIFIED_CERTIFICATION_OPTION => 'label.uncertified'
                ],
                'data'     => [
                    self::CERTIFIED_CERTIFICATION_OPTION,
                    self::UNCERTIFIED_CERTIFICATION_OPTION
                ]
            ])
        ;
    }

    /**
     * Get form name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
