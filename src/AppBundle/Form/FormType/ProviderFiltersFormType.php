<?php

namespace AppBundle\Form\FormType;

use AppBundle\AppBundle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Provider filters form
 */
class ProviderFiltersFormType extends AbstractType
{
    const NAME         = 'ProviderFiltersFormType';
    const NAME_SPACE   = AppBundle::FORM_FORM_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.form.type.provider_filters';

    const HIDE_FIELDS_OPTION      = 'hide_fields';
    const PREFERRED_ONLY_FIELD    = 'preferred_only';
    const NETWORK_FIELD           = 'network';
    const SORT_BY_FIELD           = 'sort_by';
    const CORE_NETWORK_OPTION     = 'Core';
    const EXTENDED_NETWORK_OPTION = 'Extended';
    const DISTANCE_SORT_OPTION    = 'Distance';
    const RANK_SORT_OPTION        = 'Rank';


    /**
     * Configure form fields
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(self::PREFERRED_ONLY_FIELD, 'checkbox', [
                'required' => false
            ]);

        if($options[self::HIDE_FIELDS_OPTION])
        {
            $builder
                ->add(self::NETWORK_FIELD, 'collection', [
                    'type' => 'hidden',
                    'data' => [self::CORE_NETWORK_OPTION]
                ])
                ->add(self::SORT_BY_FIELD, 'hidden', ['data' => self::RANK_SORT_OPTION])
            ;
        } else {
            $builder->add(self::NETWORK_FIELD, 'choice', [
                'expanded' => true,
                'multiple' => true,
                'data'     => [self::CORE_NETWORK_OPTION],
                'choices'  => [
                    self::CORE_NETWORK_OPTION     => 'label.core',
                    self::EXTENDED_NETWORK_OPTION => 'label.extended'
                ]
            ])
                ->add(self::SORT_BY_FIELD, 'choice', [
                    'expanded' => true,
                    'data'     => self::RANK_SORT_OPTION,
                    'choices'  => [
                        self::RANK_SORT_OPTION     => 'label.rank',
                        self::DISTANCE_SORT_OPTION => 'label.distance'
                    ]
                ])
            ;
        }
    }

    /**
     * Configure form options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([self::HIDE_FIELDS_OPTION => false]);
    }

    /**
     * Get form name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
