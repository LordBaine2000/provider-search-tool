<?php

namespace AppBundle\Form\FormType;

use AppBundle\AppBundle;
use AppBundle\Entity\AncillaryType;
use AppBundle\Entity\FacilityType;
use AppBundle\Entity\PlanType;
use AppBundle\Entity\ProviderType;
use AppBundle\Entity\SpecialtyType;
use AppBundle\Form\FieldType\HiddenEntityFieldType;
use AppBundle\Repository\BaseRepository;
use AppBundle\Repository\FacilityTypeRepository;
use AppBundle\Repository\ProviderTypeRepository;
use AppBundle\Service\LocationService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Provider search form
 */
class ProviderSearchFormType extends AbstractType
{
    const NAME         = 'ProviderSearchFormType';
    const NAME_SPACE   = AppBundle::FORM_FORM_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.form.type.provider_search';

    const HIDE_FIELDS_OPTION   = 'hide_fields';
    const PLAN_TYPE_FIELD      = PlanType::NAME;
    const PROVIDER_TYPE_FIELD  = ProviderType::NAME;
    const SPECIALTY_TYPE_FIELD = SpecialtyType::NAME;
    const FACILITY_TYPE_FIELD  = FacilityType::NAME;
    const ANCILLARY_TYPE_FIELD = AncillaryType::NAME;


    /** @var ProviderTypeRepository */
    private $providerTypeRepository;

    /** @var BaseRepository */
    private $genderTypeRepository;

    /** @var BaseRepository */
    private $languageTypeRepository;

    /** @var BaseRepository */
    private $postcodeRepository;

    /** @var LocationService */
    private $locationService;


    /**
     * Constructor
     *
     * @param ProviderTypeRepository $providerTypeRepository
     * @param BaseRepository $genderTypeRepository
     * @param BaseRepository $languageTypeRepository
     * @param BaseRepository $postcodeRepository
     * @param LocationService $locationService
     */
    public function __construct(
        ProviderTypeRepository $providerTypeRepository,
        BaseRepository $genderTypeRepository,
        BaseRepository $languageTypeRepository,
        BaseRepository $postcodeRepository,
        LocationService $locationService
    ) {
        $this->providerTypeRepository = $providerTypeRepository;
        $this->genderTypeRepository   = $genderTypeRepository;
        $this->languageTypeRepository = $languageTypeRepository;
        $this->postcodeRepository     = $postcodeRepository;
        $this->locationService        = $locationService;
    }

    /**
     * Configure form fields
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($options[self::HIDE_FIELDS_OPTION])
        {
            $builder
                ->add(self::PLAN_TYPE_FIELD, HiddenEntityFieldType::NAME, [
                    HiddenEntityFieldType::CLASS_OPTION => PlanType::ENTITY_NAME
                ])
                ->add(self::PROVIDER_TYPE_FIELD, HiddenEntityFieldType::NAME, [
                    HiddenEntityFieldType::CLASS_OPTION => ProviderType::ENTITY_NAME
                ])
                ->add(self::SPECIALTY_TYPE_FIELD, HiddenEntityFieldType::NAME, [
                    HiddenEntityFieldType::CLASS_OPTION => SpecialtyType::ENTITY_NAME
                ])
                ->add(self::FACILITY_TYPE_FIELD, HiddenEntityFieldType::NAME, [
                    HiddenEntityFieldType::CLASS_OPTION => FacilityType::ENTITY_NAME
                ])
                ->add(self::ANCILLARY_TYPE_FIELD, HiddenEntityFieldType::NAME, [
                    HiddenEntityFieldType::CLASS_OPTION => AncillaryType::ENTITY_NAME
                ])
            ;
        } else {
            $builder
                ->add(self::PLAN_TYPE_FIELD,     'entity', ['class' => PlanType::ENTITY_NAME, 'choice_label' => 'name'])
                ->add(self::PROVIDER_TYPE_FIELD, 'entity', [
                    'class' => ProviderType::ENTITY_NAME,
                    'choice_label' => 'name',
                    'query_builder' => function (ProviderTypeRepository $repository) {
                        return $repository->filterDentalQB();
                    }
                ])
                ->addEventListener(
                    FormEvents::PRE_SUBMIT,
                    [$this, 'addFields']
                )
                ->addEventListener(
                    FormEvents::SUBMIT,
                    [$this, 'removeFields']
                );
        }

        $builder
            ->add(LocationFormType::NAME, new LocationFormType($this->postcodeRepository, $this->locationService))
            ->add(ProviderFiltersFormType::NAME, new ProviderFiltersFormType(), [
                ProviderFiltersFormType::HIDE_FIELDS_OPTION => $options[self::HIDE_FIELDS_OPTION]
            ])
        ;
    }

    /**
     * Add all form fields in PRE_SUBMIT so they can be bound
     *
     * @param FormEvent $event
     */
    public function addFields(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $form
            ->add(self::SPECIALTY_TYPE_FIELD, 'entity', ['class' => SpecialtyType::ENTITY_NAME, 'choice_label' => 'name'])
            ->add(self::ANCILLARY_TYPE_FIELD, 'entity', ['class' => AncillaryType::ENTITY_NAME, 'choice_label' => 'name'])
        ;

        /** @var ProviderType $providerType */
        if($providerType = $this->providerTypeRepository->find($data[self::PROVIDER_TYPE_FIELD]))
        {
            $event->setData($this->addFacilityType($providerType, $form, $data));

            if($providerType->getSlug() == ProviderType::PRIMARY_SLUG)
            {
                $form->add(PersonFiltersFormType::NAME, new PersonFiltersFormType(
                    $this->genderTypeRepository,
                    $this->languageTypeRepository
                ));
            }
        }
    }

    /**
     * Adds either a hidden or entity field to the form depending on business logic
     *
     * @param ProviderType $providerType
     * @param FormInterface $form
     * @param array $data
     * @return array Updated form data
     */
    private function addFacilityType(ProviderType $providerType, FormInterface $form, array $data)
    {
        if($providerType instanceof FacilityType)
        {
            $form->add(self::FACILITY_TYPE_FIELD, HiddenEntityFieldType::NAME, [
                HiddenEntityFieldType::CLASS_OPTION => FacilityType::ENTITY_NAME
            ]);

            $data[self::FACILITY_TYPE_FIELD] = $data[self::PROVIDER_TYPE_FIELD];
        } else {
            $form->add(self::FACILITY_TYPE_FIELD,  'entity', [
                'class' => FacilityType::ENTITY_NAME,
                'choice_label' => 'name',
                'query_builder' => function (FacilityTypeRepository $repository) {
                    return $repository->getSubtypesQB();
                }
            ]);
        }

        return $data;
    }

    /**
     * Remove unnecessary form fields after being bound
     *
     * @param FormEvent $event
     * @throws \LogicException
     */
    public function removeFields(FormEvent $event)
    {
        $providerType = $event->getData()[self::PROVIDER_TYPE_FIELD];

        if(!($providerType instanceof ProviderType))
        {
            throw new \LogicException(sprintf('%s expected an instance of %s, but got %s instead',
                self::NAME,
                ProviderType::NAME,
                get_class($providerType)
            ));
        }

        switch($providerType->getSlug())
        {
            case ProviderType::PRIMARY_SLUG:
                $event->getForm()
                    ->remove(FacilityType::NAME)
                    ->remove(AncillaryType::NAME)
                ;
                break;
            case ProviderType::ANCILLARY_SLUG:
                $event->getForm()
                    ->remove(SpecialtyType::NAME)
                    ->remove(FacilityType::NAME)
                ;
                break;
            default:
                $event->getForm()
                    ->remove(SpecialtyType::NAME)
                    ->remove(AncillaryType::NAME)
                ;
        }
    }

    /**
     * Configure form options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([self::HIDE_FIELDS_OPTION => false]);
    }

    /**
     * Get form name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
