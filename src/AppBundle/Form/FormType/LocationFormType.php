<?php

namespace AppBundle\Form\FormType;

use AppBundle\AppBundle;
use AppBundle\Entity\Postcode;
use AppBundle\Form\FieldType\PostcodeFieldType;
use AppBundle\Repository\BaseRepository;
use AppBundle\Service\LocationService;
use Geocoder\Exception\NoResultException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * Location selection form
 *
 * @TODO: Figure out why postcode is set to 0 when API call fails
 */
class LocationFormType extends AbstractType
{
    const NAME           = 'LocationFormType';
    const NAME_SPACE     = AppBundle::FORM_FORM_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME   = 'app.form.type.location';

    const POSTCODE_FIELD = Postcode::NAME;
    const RADIUS_FIELD   = 'radius';


    /** @var BaseRepository */
    private $postcodeRepository;
    /** @var LocationService */
    private $locationService;


    /**
     * Constructor
     *
     * @param $postcodeRepository
     * @param $locationService
     */
    public function __construct($postcodeRepository, $locationService)
    {
        $this->postcodeRepository = $postcodeRepository;
        $this->locationService    = $locationService;
    }

    /**
     * Configure form fields
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(self::POSTCODE_FIELD, PostcodeFieldType::NAME, [
                'error_bubbling' => true,
                'constraints'    => new NotBlank([
                    'message' => 'label.error.postcode_blank'
                ])
            ])
            ->add(self::RADIUS_FIELD, 'choice', [
                'choices' => [
                    15 => '15 miles',
                    25 => '25 miles',
                    30 => '30 miles'
                ]
            ]);

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            [$this, 'validatePostcode']
        );
    }

    /**
     * Ensures the Postcode exists and has an associated Location
     *
     * @param FormEvent $event
     * @throws \Exception
     */
    public function validatePostcode(FormEvent $event)
    {
        $data = $event->getData();

        try {
            if(!$postcode = $this->postcodeRepository->findOneByName($data[self::POSTCODE_FIELD]))
            {
                $postcode = new Postcode();
                $postcode->setName($data[self::POSTCODE_FIELD]);

                $this->postcodeRepository->persist($postcode);
            }

            $this->locationService->locatePostcode($postcode);
        } catch(\Exception $e) {
            if(!($e instanceof ValidatorException) && !($e instanceof NoResultException)) throw $e;

            $event->getForm()->get(self::POSTCODE_FIELD)->addError(new FormError('label.error.postcode_invalid'));
        }
    }

    /**
     * Get form name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
