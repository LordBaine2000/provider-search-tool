<?php

namespace AppBundle\Form\FormType;

use AppBundle\AppBundle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Dynamic form for Entity selection
 */
class DynamicEntitySelectionFormType extends AbstractType
{
    const NAME         = 'DynamicEntitySelectionFormType';
    const NAME_SPACE   = AppBundle::FORM_FORM_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.form.type.dynamic_entity_selection';

    const CLASS_OPTION         = 'class';
    const QUERY_BUILDER_OPTION = 'query_builder';


    /**
     * Configure form fields
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add($options[self::CLASS_OPTION], 'entity', [
                'class' => AppBundle::NAME.':'.$options[self::CLASS_OPTION],
                'choice_label' => 'name',
                self::QUERY_BUILDER_OPTION => $options[self::QUERY_BUILDER_OPTION]
            ]);
    }

    /**
     * Configure form options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            self::CLASS_OPTION         => null,
            self::QUERY_BUILDER_OPTION => null
        ]);
    }

    /**
     * Get form name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
