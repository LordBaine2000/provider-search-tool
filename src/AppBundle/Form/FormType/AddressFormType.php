<?php

namespace AppBundle\Form\FormType;

use AppBundle\AppBundle;
use AppBundle\Entity\Postcode;
use AppBundle\Entity\RegionIsoCodeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Address form
 */
class AddressFormType extends AbstractType
{
    const NAME         = 'AddressFormType';
    const NAME_SPACE   = AppBundle::FORM_FORM_TYPE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.form.type.address';

    const STREET1_FIELD  = 'street1';
    const STREET2_FIELD  = 'street2';
    const CITY_FIELD     = 'city';
    const REGION_FIELD   = RegionIsoCodeType::NAME;
    const POSTCODE_FIELD = Postcode::NAME;


    /**
     * Configure form fields
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(self::STREET1_FIELD, 'text', [
                'error_bubbling' => true,
                'constraints'    => new NotBlank([
                    'message' => 'label.error.street1_blank'
                ])
            ])
            ->add(self::STREET2_FIELD, 'text', [
                'required' => false
            ])
            ->add(self::CITY_FIELD, 'text', [
                'error_bubbling' => true,
                'constraints'    => new NotBlank([
                    'message' => 'label.error.city_blank'
                ])
            ])
            ->add(self::REGION_FIELD, 'entity', [
                'class' => RegionIsoCodeType::NAME_SPACE
            ])
            ->add(self::POSTCODE_FIELD, 'integer', [
                'error_bubbling' => true,
                'constraints'    => new NotBlank([
                    'message' => 'label.error.postcode_blank'
                ])
            ])
        ;
    }

    /**
     * Get form name
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
