<?php

namespace AppBundle\Form\DataTransformer;

use AppBundle\AppBundle;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Transforms entity to/from it's identifier
 */
class EntityTransformer implements DataTransformerInterface
{
    const NAME       = 'EntityTransformer';
    const NAME_SPACE = AppBundle::FORM_DATA_TRANSFORMER_NAMESPACE.self::NAME;


    /** @var EntityManager */
    private $em;

    /** @var string */
    private $class;

    /** @var string */
    private $property;


    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param string        $class
     * @param string        $property
     */
    public function __construct(EntityManager $em, $class, $property)
    {
        $this->em       = $em;
        $this->class    = $class;
        $this->property = strtolower($property);
    }

    /**
     * Convert entity to identifier
     *
     * @param  mixed $entity
     * @return string
     */
    public function transform($entity)
    {
        $getter = 'get'.ucfirst($this->property);

        return method_exists($entity, $getter) ? $entity->$getter() : null;
    }

    /**
     * Convert identifier to entity
     *
     * @param  string $identifier
     * @return mixed
     */
    public function reverseTransform($identifier)
    {
        $entity = $this->em->getRepository($this->class)->findOneBy([
            $this->property => $identifier
        ]);

        return ($entity) ?: $identifier;
    }
}
