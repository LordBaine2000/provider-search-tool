<?php

namespace AppBundle\DependencyInjection;

use AppBundle\AppBundle;
use AppBundle\Entity\Address;
use AppBundle\Entity\Ancillary;
use AppBundle\Entity\Facility;
use AppBundle\Entity\FacilityType;
use AppBundle\Entity\GenderType;
use AppBundle\Entity\LanguageType;
use AppBundle\Entity\LocationType;
use AppBundle\Entity\Person;
use AppBundle\Entity\Postcode;
use AppBundle\Entity\ProviderType;
use AppBundle\Form\FieldType\HiddenEntityFieldType;
use AppBundle\Form\FieldType\PostcodeFieldType;
use AppBundle\Form\FormType\AddressFormType;
use AppBundle\Form\FormType\DynamicEntitySelectionFormType;
use AppBundle\Form\FormType\LocationFormType;
use AppBundle\Form\FormType\PersonFiltersFormType;
use AppBundle\Form\FormType\ProviderFiltersFormType;
use AppBundle\Form\FormType\ProviderSearchFormType;
use AppBundle\Form\FormType\SuggestProviderFormType;
use AppBundle\Geocoder\Provider\GoogleMapsProvider;
use AppBundle\Repository\AncillaryRepository;
use AppBundle\Repository\BaseRepository;
use AppBundle\Repository\FacilityRepository;
use AppBundle\Repository\FacilityTypeRepository;
use AppBundle\Repository\PersonRepository;
use AppBundle\Repository\ProviderTypeRepository;
use AppBundle\Service\LocationService;
use AppBundle\Service\ProviderRepositoryFactory;
use AppBundle\Service\ProviderRouteFactory;
use AppBundle\Service\ProviderSearchService;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Dynamically load service names from class constants
 */
class AppExtension extends Extension
{
    const NAME       = 'AppExtension';
    const NAME_SPACE = AppBundle::DEPENDENCY_INJECTION_NAMESPACE.self::NAME;

    const BAZINGA_GEOCODER_SERVICE_NAME        = 'bazinga_geocoder.geocoder';
    const SYMFONY_VALIDATOR_SERVICE_NAME       = 'validator';
    const DOCTRINE_ENTITY_MANAGER_SERVICE_NAME = 'doctrine.orm.entity_manager';


    /** @var array */
    private static $service_configuration = [
        // Application Services
        ProviderRepositoryFactory::SERVICE_NAME => [
            'class'   => ProviderRepositoryFactory::NAME_SPACE,
            'methods' => [
                'setAncillaryRepository' => ['@'.AncillaryRepository::SERVICE_NAME],
                'setFacilityRepository'  => ['@'.FacilityRepository::SERVICE_NAME],
                'setPersonRepository'    => ['@'.PersonRepository::SERVICE_NAME]
            ]
        ],
        ProviderRouteFactory::SERVICE_NAME => [
            'class' => ProviderRouteFactory::NAME_SPACE
        ],
        ProviderSearchService::SERVICE_NAME => [
            'class'   => ProviderSearchService::NAME_SPACE,
            'methods' => [
                'setFactory' => ['@'.ProviderRepositoryFactory::SERVICE_NAME]
            ]
        ],
        LocationService::SERVICE_NAME => [
            'class'   => LocationService::NAME_SPACE,
            'methods' => [
                'setGeocoder'       => ['@'.self::BAZINGA_GEOCODER_SERVICE_NAME],
                'setRepository'     => ['@'.BaseRepository::LOCATION_TYPE_REPOSITORY_SERVICE_NAME],
                'setGoogleProvider' => ['@'.GoogleMapsProvider::SERVICE_NAME]
            ]
        ],
        // Repositories
        AncillaryRepository::SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [Ancillary::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        FacilityRepository::SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [Facility::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        PersonRepository::SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [Person::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        ProviderTypeRepository::SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [ProviderType::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        FacilityTypeRepository::SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [FacilityType::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        BaseRepository::LOCATION_TYPE_REPOSITORY_SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [LocationType::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        BaseRepository::POSTCODE_REPOSITORY_SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [Postcode::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        BaseRepository::ADDRESS_REPOSITORY_SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [Address::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        BaseRepository::GENDER_TYPE_REPOSITORY_SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [GenderType::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        BaseRepository::LANGUAGE_TYPE_REPOSITORY_SERVICE_NAME => [
            'class'     => 'Doctrine\Common\Persistence\ObjectRepository',
            'factory'   => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME, 'getRepository'],
            'arguments' => [LanguageType::NAME_SPACE],
            'methods'   => [
                'setValidator' => ['@'.self::SYMFONY_VALIDATOR_SERVICE_NAME]
            ]
        ],
        // Form types
        DynamicEntitySelectionFormType::SERVICE_NAME => [
            'class' => DynamicEntitySelectionFormType::NAME_SPACE,
            'tags'  => [
                'form.type' => ['alias' => DynamicEntitySelectionFormType::NAME]
            ]
        ],
        LocationFormType::SERVICE_NAME => [
            'class' => LocationFormType::NAME_SPACE,
            'arguments' => [
                '@'.BaseRepository::POSTCODE_REPOSITORY_SERVICE_NAME,
                '@'.LocationService::SERVICE_NAME
            ],
            'tags'  => [
                'form.type' => ['alias' => LocationFormType::NAME]
            ]
        ],
        PersonFiltersFormType::NAME_SPACE => [
            'class'     => PersonFiltersFormType::NAME_SPACE,
            'arguments' => [
                '@'.BaseRepository::GENDER_TYPE_REPOSITORY_SERVICE_NAME,
                '@'.BaseRepository::LANGUAGE_TYPE_REPOSITORY_SERVICE_NAME
            ],
            'tags'      => [
                'form.type' => ['alias' => ProviderSearchFormType::NAME]
            ]
        ],
        ProviderFiltersFormType::NAME_SPACE => [
            'class'     => PersonFiltersFormType::NAME_SPACE,
            'tags'      => [
                'form.type' => ['alias' => ProviderSearchFormType::NAME]
            ]
        ],
        ProviderSearchFormType::SERVICE_NAME => [
            'class'     => ProviderSearchFormType::NAME_SPACE,
            'arguments' => [
                '@'.ProviderTypeRepository::SERVICE_NAME,
                '@'.BaseRepository::GENDER_TYPE_REPOSITORY_SERVICE_NAME,
                '@'.BaseRepository::LANGUAGE_TYPE_REPOSITORY_SERVICE_NAME,
                '@'.BaseRepository::POSTCODE_REPOSITORY_SERVICE_NAME,
                '@'.LocationService::SERVICE_NAME
            ],
            'tags'      => [
                'form.type' => ['alias' => ProviderSearchFormType::NAME]
            ]
        ],
        SuggestProviderFormType::SERVICE_NAME => [
            'class' => SuggestProviderFormType::NAME_SPACE,
            'arguments' => ['@'.ProviderTypeRepository::SERVICE_NAME],
            'tags'  => [
                'form.type' => ['alias' => SuggestProviderFormType::NAME]
            ]
        ],
        AddressFormType::SERVICE_NAME => [
            'class' => AddressFormType::NAME_SPACE,
            'tags'  => [
                'form.type' => ['alias' => AddressFormType::NAME]
            ]
        ],
        // Field types
        HiddenEntityFieldType::SERVICE_NAME => [
            'class'     => HiddenEntityFieldType::NAME_SPACE,
            'arguments' => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME],
            'tags'      => [
                'form.type' => ['alias' => HiddenEntityFieldType::NAME]
            ]
        ],
        PostcodeFieldType::SERVICE_NAME => [
            'class'     => PostcodeFieldType::NAME_SPACE,
            'arguments' => ['@'.self::DOCTRINE_ENTITY_MANAGER_SERVICE_NAME],
            'tags'      => [
                'form.type' => ['alias' => PostcodeFieldType::NAME]
            ]
        ]
    ];


    /**
     * @inheritdoc
     * @throws \UnexpectedValueException
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        foreach(self::$service_configuration as $service => $configs)
        {
            $definition = new Definition();

            foreach($configs as $name => $value)
            {
                switch($name)
                {
                    case 'class':
                        $definition->setClass($value);
                        break;
                    case 'factory':
                        $definition->setFactory($this->resolveReferences($value));
                        break;
                    case 'methods':
                        $this->addMethodCalls($definition, $value);
                        break;
                    case 'arguments':
                        $this->addArguments($definition, $value);
                        break;
                    case 'tags':
                        $this->addTags($definition, $value);
                        break;
                    default:
                        throw new \UnexpectedValueException("Unexpected key '$name' in '$service' configuration");
                }
            }

            $container->setDefinition($service, $definition);
        }
    }

    /**
     * Adds method calls to service definition
     *
     * @param Definition $definition
     * @param array $methodArray
     */
    private function addMethodCalls(Definition $definition, array $methodArray)
    {
        foreach($methodArray as $method => $arguments)
        {
            $definition->addMethodCall($method, $this->resolveReferences($arguments));
        }
    }

    /**
     * Adds arguments to service definition
     *
     * @param Definition $definition
     * @param array $argumentArray
     */
    private function addArguments(Definition $definition, array $argumentArray)
    {
        foreach($argumentArray as $argument)
        {
            $definition->addArgument($this->resolveReference($argument));
        }
    }

    /**
     * Adds tags to a service definition
     *
     * @param Definition $definition
     * @param array $tagArray
     */
    private function addTags(Definition $definition, array $tagArray)
    {
        foreach($tagArray as $tag => $attributes)
        {
            $definition->addTag($tag, $attributes);
        }
    }

    /**
     * Parses arguments for service definitions and returns Reference object if found
     *
     * @param  string $argument
     * @return string|Reference
     */
    private function resolveReference($argument)
    {
        return (substr($argument, 0, 1) === '@') ? new Reference(substr($argument, 1)) : $argument;
    }

    /**
     * Resolves all service references inside an array
     *
     * @param  array $argumentArray
     * @return array
     */
    private function resolveReferences(array $argumentArray)
    {
        foreach($argumentArray as $key => $argument)
        {
            $argumentArray[$key] = $this->resolveReference($argument);
        }

        return $argumentArray;
    }
}
