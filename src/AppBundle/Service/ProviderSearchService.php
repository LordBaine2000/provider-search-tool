<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Form\FormType\ProviderSearchFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

/**
 * Houses domain logic for searching Providers
 */
class ProviderSearchService
{
    const NAME         = 'ProviderSearchService';
    const NAME_SPACE   = AppBundle::SERVICE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.service.provider_search';


    /** @var ProviderRepositoryFactory */
    private $factory;


    /**
     * Perform Provider search
     *
     * @param Form $form
     * @return ArrayCollection <Provider>
     */
    public function search(Form $form)
    {
        if($form->isSubmitted() && $form->isValid())
        {
            return $this->factory->get($form->get(ProviderSearchFormType::PROVIDER_TYPE_FIELD)->getData())
                ->findBySearchForm($form);
        } else {
            return new ArrayCollection();
        }
    }

    /**
     * Set factory
     *
     * @param ProviderRepositoryFactory $factory
     * @return $this
     */
    public function setFactory(ProviderRepositoryFactory $factory)
    {
        $this->factory = $factory;

        return $this;
    }
}
