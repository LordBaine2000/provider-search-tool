<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Entity\FacilityType;
use AppBundle\Entity\ProviderType;
use AppBundle\Repository\AbstractProviderRepository;
use AppBundle\Repository\AncillaryRepository;
use AppBundle\Repository\FacilityRepository;
use AppBundle\Repository\PersonRepository;

/**
 * Houses domain logic for returning Provider Repositories
 */
class ProviderRepositoryFactory
{
    const NAME         = 'ProviderRepositoryFactory';
    const NAME_SPACE   = AppBundle::SERVICE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.service.factory.provider_repository';


    /** @var AncillaryRepository */
    private $ancillaryRepository;

    /** @var FacilityRepository */
    private $facilityRepository;

    /** @var PersonRepository */
    private $personRepository;


    /**
     * Get ProviderRepositoryInterface by ProviderType
     *
     * @param ProviderType $providerType
     * @return AbstractProviderRepository
     * @throws \UnexpectedValueException
     */
    public function get(ProviderType $providerType)
    {
        switch($providerType->getSlug())
        {
            case ProviderType::PRIMARY_SLUG:
                return $this->personRepository;
                break;
            case ProviderType::ANCILLARY_SLUG:
                return $this->ancillaryRepository;
                break;
            case FacilityType::VISION_SLUG:
            case ProviderType::HOSPITAL_SLUG:
            case FacilityType::LAB_SLUG:
            case FacilityType::PHARMACY_SLUG:
                return $this->facilityRepository;
                break;
            default:
                throw new \UnexpectedValueException(sprintf(
                    'Unknown %s slug: %s',
                        ProviderType::NAME,
                        $providerType->getSlug()
                ));
        }
    }

    /**
     * Set ancillaryRepository
     *
     * @param AncillaryRepository $ancillaryRepository
     * @return $this
     */
    public function setAncillaryRepository(AncillaryRepository $ancillaryRepository)
    {
        $this->ancillaryRepository = $ancillaryRepository;

        return $this;
    }

    /**
     * Set facilityRepository
     *
     * @param FacilityRepository $facilityRepository
     * @return $this
     */
    public function setFacilityRepository(FacilityRepository $facilityRepository)
    {
        $this->facilityRepository = $facilityRepository;

        return $this;
    }

    /**
     * Set personRepository
     *
     * @param PersonRepository $personRepository
     * @return $this
     */
    public function setPersonRepository(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;

        return $this;
    }
}
