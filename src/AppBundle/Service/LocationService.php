<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Entity\Address;
use AppBundle\Entity\LocationType;
use AppBundle\Entity\Postcode;
use AppBundle\Geocoder\Provider\GoogleMapsProvider;
use AppBundle\Model\LocationInterface;
use AppBundle\Repository\BaseRepository;
use CrEOF\Spatial\PHP\Types\Geography\Point;
use Geocoder\Geocoder;
use Geocoder\Result\ResultInterface;

/**
 * Houses domain logic for dealing with LocationTypes
 */
class LocationService
{
    const NAME         = 'LocationService';
    const NAME_SPACE   = AppBundle::SERVICE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.service.location';


    /** @var GoogleMapsProvider */
    private $googleProvider;

    /** @var Geocoder */
    private $geocoder;

    /** @var BaseRepository */
    private $repository;

    /** @var array */
    private $componentFilters = ['country' => 'US'];


    /**
     * Creates and saves a LocationType for a Postcode using Google's Geocoding API if one doesn't exist
     *
     * @param Postcode $postcode
     * @return LocationType
     */
    public function locatePostcode(Postcode $postcode)
    {
        if(!$location = $postcode->getLocation())
        {
            $location = $this->createLocation($postcode, $this->geocode(' ', [
                'postal_code' => $postcode->getName()
            ]));
        }

        return $location;
    }

    /**
     * Creates and saves a LocationType for an Address using Google's Geocoding API if one doesn't exist
     *
     * @param Address $address
     * @return LocationType
     */
    public function locateAddress(Address $address)
    {
        if(!$location = $address->getLocation())
        {
            $location = $this->createLocation($address, $this->geocode((string) $address));
        }

        return $location;
    }

    /**
     * Performs a Google Geocode API call
     *
     * @param $value
     * @param array $additionalFilters
     * @return ResultInterface
     */
    private function geocode($value, array $additionalFilters = [])
    {
        $this->googleProvider->setComponentFilters(array_merge($this->componentFilters, $additionalFilters));

        return $this->geocoder->geocode($value);
    }

    /**
     * Creates a LocationType with the results of a Geocoder query
     *
     * @param LocationInterface $entity
     * @param ResultInterface $geo
     * @return LocationType
     */
    private function createLocation(LocationInterface $entity, ResultInterface $geo)
    {
        $location = new LocationType();
        $location->setPoint(new Point($geo->getLongitude(), $geo->getLatitude()));
        $entity->setLocation($location);

        $this->repository->persist($location);
        $this->repository->flush();

        return $location;
    }

    /**
     * Set googleProvider
     *
     * @param GoogleMapsProvider $googleProvider
     * @return $this
     */
    public function setGoogleProvider(GoogleMapsProvider $googleProvider)
    {
        $this->googleProvider = $googleProvider;

        return $this;
    }

    /**
     * Set geocoder
     *
     * @param Geocoder $geocoder
     * @return $this
     */
    public function setGeocoder(Geocoder $geocoder)
    {
        $this->geocoder = $geocoder;

        return $this;
    }

    /**
     * Set repository
     *
     * @param BaseRepository $repository
     * @return $this
     */
    public function setRepository(BaseRepository $repository)
    {
        $this->repository = $repository;

        return $this;
    }
}