<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Controller\ApplicationController;
use AppBundle\Entity\FacilityType;
use AppBundle\Entity\ProviderType;

/**
 * Houses domain logic for returning form routes
 */
class ProviderRouteFactory
{
    const NAME         = 'ProviderRouteFactory';
    const NAME_SPACE   = AppBundle::SERVICE_NAMESPACE.self::NAME;
    const SERVICE_NAME = 'app.service.factory.provider_route';


    /**
     * Get ApplicationController routes by ProviderType
     *
     * @param  ProviderType $providerType
     * @return string
     * @throws \UnexpectedValueException
     */
    public function get(ProviderType $providerType)
    {
        switch($providerType->getSlug())
        {
            case FacilityType::DENTAL_SLUG:
                $route = ApplicationController::DENTAL_ROUTE;
                break;
            case ProviderType::PRIMARY_SLUG:
                $route = ApplicationController::SPECIALTY_TYPE_ROUTE;
                break;
            case ProviderType::HOSPITAL_SLUG:
                $route = ApplicationController::FACILITY_TYPE_ROUTE;
                break;
            case ProviderType::ANCILLARY_SLUG:
                $route = ApplicationController::ANCILLARY_TYPE_ROUTE;
                break;
            case FacilityType::VISION_SLUG:
            case FacilityType::LAB_SLUG:
            case FacilityType::PHARMACY_SLUG:
                $route = ApplicationController::LOCATION_ROUTE;
                break;
            default:
                throw new \UnexpectedValueException(sprintf(
                    'Unknown %s slug: %s',
                    ProviderType::NAME,
                    $providerType->getSlug()
                ));
        }

        return $route;
    }
}
