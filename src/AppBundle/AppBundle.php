<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    const NAME = 'AppBundle';

    const COMMAND_NAMESPACE               = self::NAME.'\Command\\';
    const CONTROLLER_NAMESPACE            = self::NAME.'\Controller\\';
    const DATA_FIXTURES_NAMESPACE         = self::NAME.'\DataFixtures\\';
    const DATA_FIXTURES_ORM_NAMESPACE     = self::DATA_FIXTURES_NAMESPACE.'ORM\\';
    const DEPENDENCY_INJECTION_NAMESPACE  = self::NAME.'\DependencyInjection\\';
    const ENTITY_NAMESPACE                = self::NAME.'\Entity\\';
    const FORM_NAMESPACE                  = self::NAME.'\Form\\';
    const FORM_DATA_TRANSFORMER_NAMESPACE = self::FORM_NAMESPACE.'DataTransformer\\';
    const FORM_FIELD_TYPE_NAMESPACE       = self::FORM_NAMESPACE.'FieldType\\';
    const FORM_FORM_TYPE_NAMESPACE        = self::FORM_NAMESPACE.'FormType\\';
    const GEOCODER_NAMESPACE              = self::NAME.'\Geocoder\\';
    const GEOCODER_PROVIDER_NAMESPACE     = self::GEOCODER_NAMESPACE.'Provider\\';
    const MODEL_NAMESPACE                 = self::NAME.'\Model\\';
    const REPOSITORY_NAMESPACE            = self::NAME.'\Repository\\';
    const SERVICE_NAMESPACE               = self::NAME.'\Service\\';
}
