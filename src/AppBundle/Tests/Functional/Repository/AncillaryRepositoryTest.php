<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Entity\Ancillary;
use AppBundle\Repository\AncillaryRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AncillaryRepositoryTest extends KernelTestCase
{
    /** @var EntityManager */
    private $em;

    /** @var AncillaryRepository */
    private $ancillaryRepository;

    public function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $this->ancillaryRepository = $this->em->getRepository(Ancillary::ENTITY_NAME);
    }

    public function testFindBySearchForm()
    {
        $this->assertTrue(false);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
    }
}
