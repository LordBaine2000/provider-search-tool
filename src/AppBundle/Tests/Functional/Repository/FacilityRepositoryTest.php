<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Entity\Facility;
use AppBundle\Repository\FacilityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FacilityRepositoryTest extends KernelTestCase
{
    /** @var EntityManager */
    private $em;

    /** @var FacilityRepository */
    private $FacilityRepository;

    public function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $this->FacilityRepository = $this->em->getRepository(Facility::ENTITY_NAME);
    }

    public function testFindBySearchForm()
    {
        $this->assertTrue(false);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
    }
}
