<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Entity\Person;
use AppBundle\Repository\PersonRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PersonRepositoryTest extends KernelTestCase
{
    /** @var EntityManager */
    private $em;

    /** @var PersonRepository */
    private $PersonRepository;

    public function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $this->PersonRepository = $this->em->getRepository(Person::ENTITY_NAME);
    }

    public function testFindBySearchForm()
    {
        $this->assertTrue(false);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
    }
}
