<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Entity\ProviderType;
use AppBundle\Repository\ProviderTypeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProviderTypeRepositoryTest extends KernelTestCase
{
    /** @var EntityManager */
    private $em;

    /** @var ProviderTypeRepository */
    private $ProviderTypeRepository;

    public function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $this->ProviderTypeRepository = $this->em->getRepository(ProviderType::ENTITY_NAME);
    }

    public function testFilterDentalQB()
    {
        $this->assertTrue(false);
    }

    public function testGetMetaTypesQB()
    {
        $this->assertTrue(false);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
    }
}
