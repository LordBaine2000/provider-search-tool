<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Entity\ProviderType;
use AppBundle\Form\FormType\ProviderSearchFormType;
use AppBundle\Repository\AbstractProviderRepository;
use AppBundle\Service\ProviderRepositoryFactory;
use AppBundle\Service\ProviderSearchService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

class ProviderSearchServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject|Form */
    private $mForm;

    /** @var \PHPUnit_Framework_MockObject_MockObject|AbstractProviderRepository */
    private $mAbstractProviderRepository;

    /** @var ProviderSearchService */
    private $providerSearchService;


    public function setUp()
    {
        parent::setUp();

        $mFormField = $this->getMockBuilder('Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $mFormField
            ->expects($this->any())
            ->method('getData')
            ->willReturn(new ProviderType())
        ;

        $this->mForm = $this->getMockBuilder('Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->mForm
            ->expects($this->any())
            ->method('get')
            ->with(ProviderSearchFormType::PROVIDER_TYPE_FIELD)
            ->willReturn($mFormField)
        ;

        $this->mAbstractProviderRepository = $this->getMockBuilder(AbstractProviderRepository::NAME_SPACE)
            ->disableOriginalConstructor()
            ->setMethods(['findBySearchForm'])
            ->getMock()
        ;

        /** @var \PHPUnit_Framework_MockObject_MockObject|ProviderRepositoryFactory $mProviderRepositoryFactory */
        $mProviderRepositoryFactory = $this->getMockBuilder(ProviderRepositoryFactory::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $mProviderRepositoryFactory
            ->expects($this->any())
            ->method('get')
            ->with($this->isInstanceOf(ProviderType::NAME_SPACE))
            ->willReturn($this->mAbstractProviderRepository)
        ;

        $this->providerSearchService = new ProviderSearchService();
        $this->providerSearchService->setFactory($mProviderRepositoryFactory);
    }

    public function testSearch_Success()
    {
        $this->mForm
            ->expects($this->once())
            ->method('isSubmitted')
            ->willReturn(true)
        ;

        $this->mForm
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(true)
        ;

        $result = new ArrayCollection(['search', 'results']);

        $this->mAbstractProviderRepository
            ->expects($this->once())
            ->method('findBySearchForm')
            ->with($this->mForm)
            ->willReturn($result)
        ;

        $this->assertSame($result, $this->providerSearchService->search($this->mForm),
            'Search results were not returned as expected'
        );
    }

    public function testSearch_Unsubmitted()
    {
        $this->mForm
            ->expects($this->once())
            ->method('isSubmitted')
            ->willReturn(false)
        ;

        $this->mForm
            ->expects($this->any())
            ->method('isValid')
            ->willReturn(true)
        ;

        $this->assertEquals(new ArrayCollection(), $this->providerSearchService->search($this->mForm),
            'Empty ArrayCollection was not returned from an unsubmitted form'
        );
    }

    public function testSearch_Invalid()
    {
        $this->mForm
            ->expects($this->any())
            ->method('isSubmitted')
            ->willReturn(true)
        ;

        $this->mForm
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(false)
        ;

        $this->assertEquals(new ArrayCollection(), $this->providerSearchService->search($this->mForm),
            'Empty ArrayCollection was not returned from an invalid form'
        );
    }
}
