<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Controller\ApplicationController;
use AppBundle\Entity\FacilityType;
use AppBundle\Entity\ProviderType;
use AppBundle\Service\ProviderRouteFactory;

class ProviderRouteFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var ProviderRouteFactory */
    private $providerRouteFactory;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ProviderType */
    private $mProviderType;


    public function setUp()
    {
        parent::setUp();

        $this->mProviderType = $this->getMockBuilder(ProviderType::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock();

        $this->providerRouteFactory = new ProviderRouteFactory();
    }

    public function testGet()
    {
        $this->mProviderType
            ->expects($this->any())
            ->method('getSlug')
            ->will($this->onConsecutiveCalls(
                FacilityType::DENTAL_SLUG,
                ProviderType::PRIMARY_SLUG,
                ProviderType::HOSPITAL_SLUG,
                ProviderType::ANCILLARY_SLUG,
                FacilityType::VISION_SLUG,
                FacilityType::LAB_SLUG,
                FacilityType::PHARMACY_SLUG
            ))
        ;

        $this->assertEquals(ApplicationController::DENTAL_ROUTE, $this->providerRouteFactory->get($this->mProviderType),
            sprintf('%s did not return %s route from %s slug as expected',
                ProviderRouteFactory::NAME, ApplicationController::DENTAL_ROUTE, FacilityType::DENTAL_SLUG
            )
        );

        $this->assertEquals(ApplicationController::SPECIALTY_TYPE_ROUTE, $this->providerRouteFactory->get($this->mProviderType),
            sprintf('%s did not return %s route from %s slug as expected',
                ProviderRouteFactory::NAME, ApplicationController::SPECIALTY_TYPE_ROUTE, ProviderType::PRIMARY_SLUG
            )
        );

        $this->assertEquals(ApplicationController::FACILITY_TYPE_ROUTE, $this->providerRouteFactory->get($this->mProviderType),
            sprintf('%s did not return %s route from %s slug as expected',
                ProviderRouteFactory::NAME, ApplicationController::FACILITY_TYPE_ROUTE, ProviderType::HOSPITAL_SLUG
            )
        );

        $this->assertEquals(ApplicationController::ANCILLARY_TYPE_ROUTE, $this->providerRouteFactory->get($this->mProviderType),
            sprintf('%s did not return %s route from %s slug as expected',
                ProviderRouteFactory::NAME, ApplicationController::ANCILLARY_TYPE_ROUTE, ProviderType::ANCILLARY_SLUG
            )
        );

        $this->assertEquals(ApplicationController::LOCATION_ROUTE, $this->providerRouteFactory->get($this->mProviderType),
            sprintf('%s did not return %s route from %s slug as expected',
                ProviderRouteFactory::NAME, ApplicationController::LOCATION_ROUTE, FacilityType::VISION_SLUG
            )
        );

        $this->assertEquals(ApplicationController::LOCATION_ROUTE, $this->providerRouteFactory->get($this->mProviderType),
            sprintf('%s did not return %s route from %s slug as expected',
                ProviderRouteFactory::NAME, ApplicationController::LOCATION_ROUTE, FacilityType::LAB_SLUG
            )
        );

        $this->assertEquals(ApplicationController::LOCATION_ROUTE, $this->providerRouteFactory->get($this->mProviderType),
            sprintf('%s did not return %s route from %s slug as expected',
                ProviderRouteFactory::NAME, ApplicationController::LOCATION_ROUTE, FacilityType::PHARMACY_SLUG
            )
        );
    }

    /** @expectedException \UnexpectedValueException */
    public function testException()
    {
        $this->mProviderType
            ->expects($this->any())
            ->method('getSlug')
            ->willReturn('invalid-slug')
        ;

        $this->providerRouteFactory->get($this->mProviderType);
    }
}
