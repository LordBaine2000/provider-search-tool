<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Entity\FacilityType;
use AppBundle\Entity\ProviderType;
use AppBundle\Repository\AncillaryRepository;
use AppBundle\Repository\FacilityRepository;
use AppBundle\Repository\PersonRepository;
use AppBundle\Service\ProviderRepositoryFactory;

class ProviderRepositoryFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var ProviderRepositoryFactory */
    private $providerRepositoryFactory;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ProviderType */
    private $mProviderType;


    public function setUp()
    {
        parent::setUp();

        /** @var AncillaryRepository $mAncillaryRepository */
        $mAncillaryRepository = $this->getMockBuilder(AncillaryRepository::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var FacilityRepository $mFacilityRepository */
        $mFacilityRepository = $this->getMockBuilder(FacilityRepository::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var PersonRepository $mPersonRepository */
        $mPersonRepository = $this->getMockBuilder(PersonRepository::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mProviderType = $this->getMockBuilder(ProviderType::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock();

        $this->providerRepositoryFactory = new ProviderRepositoryFactory();
        $this->providerRepositoryFactory
            ->setAncillaryRepository($mAncillaryRepository)
            ->setFacilityRepository($mFacilityRepository)
            ->setPersonRepository($mPersonRepository)
        ;
    }

    public function testGet()
    {
        $this->mProviderType
            ->expects($this->any())
            ->method('getSlug')
            ->will($this->onConsecutiveCalls(
                ProviderType::PRIMARY_SLUG,
                ProviderType::ANCILLARY_SLUG,
                ProviderType::HOSPITAL_SLUG,
                FacilityType::VISION_SLUG,
                FacilityType::LAB_SLUG,
                FacilityType::PHARMACY_SLUG
            ))
        ;

        $this->assertInstanceOf(PersonRepository::NAME_SPACE, $this->providerRepositoryFactory->get($this->mProviderType),
            sprintf('%s did not return %s from %s as expected',
                ProviderRepositoryFactory::NAME, PersonRepository::NAME, ProviderType::PRIMARY_SLUG
            )
        );

        $this->assertInstanceOf(AncillaryRepository::NAME_SPACE, $this->providerRepositoryFactory->get($this->mProviderType),
            sprintf('%s did not return %s from %s as expected',
                ProviderRepositoryFactory::NAME, AncillaryRepository::NAME, ProviderType::ANCILLARY_SLUG
            )
        );

        $this->assertInstanceOf(FacilityRepository::NAME_SPACE, $this->providerRepositoryFactory->get($this->mProviderType),
            sprintf('%s did not return %s from %s as expected',
                ProviderRepositoryFactory::NAME, FacilityRepository::NAME, ProviderType::HOSPITAL_SLUG
            )
        );

        $this->assertInstanceOf(FacilityRepository::NAME_SPACE, $this->providerRepositoryFactory->get($this->mProviderType),
            sprintf('%s did not return %s from %s as expected',
                ProviderRepositoryFactory::NAME, FacilityRepository::NAME, FacilityType::VISION_SLUG
            )
        );

        $this->assertInstanceOf(FacilityRepository::NAME_SPACE, $this->providerRepositoryFactory->get($this->mProviderType),
            sprintf('%s did not return %s from %s as expected',
                ProviderRepositoryFactory::NAME, FacilityRepository::NAME, FacilityType::LAB_SLUG
            )
        );

        $this->assertInstanceOf(FacilityRepository::NAME_SPACE, $this->providerRepositoryFactory->get($this->mProviderType),
            sprintf('%s did not return %s from %s as expected',
                ProviderRepositoryFactory::NAME, FacilityRepository::NAME, FacilityType::PHARMACY_SLUG
            )
        );
    }

    /** @expectedException \UnexpectedValueException */
    public function testException()
    {
        $this->mProviderType
            ->expects($this->any())
            ->method('getSlug')
            ->willReturn('invalid-slug')
        ;

        $this->providerRepositoryFactory->get($this->mProviderType);
    }
}
