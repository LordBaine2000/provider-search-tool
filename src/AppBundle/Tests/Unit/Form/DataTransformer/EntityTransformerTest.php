<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Entity\ProviderType;
use AppBundle\Form\DataTransformer\EntityTransformer;
use AppBundle\Repository\ProviderTypeRepository;
use Doctrine\ORM\EntityManager;

class EntityTransformerTest extends \PHPUnit_Framework_TestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject|EntityManager */
    private $mEntityManager;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ProviderTypeRepository */
    private $mProviderTypeRepository;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ProviderType */
    private $mProviderType;


    public function setUp()
    {
        parent::setUp();

        $this->mProviderType = $this->getMockBuilder(ProviderType::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->mProviderType
            ->expects($this->any())
            ->method('getName')
            ->willReturn('Test Entity')
        ;

        $this->mProviderType
            ->expects($this->any())
            ->method('getSlug')
            ->willReturn('test-entity')
        ;

        $this->mProviderTypeRepository = $this->getMockBuilder(ProviderTypeRepository::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->mEntityManager = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->mEntityManager
            ->expects($this->any())
            ->method('getRepository')
            ->with(ProviderType::NAME_SPACE)
            ->willReturn($this->mProviderTypeRepository)
        ;
    }

    public function testTransform()
    {
        $entityTransformer = new EntityTransformer($this->mEntityManager, ProviderType::NAME_SPACE, 'name');
        $this->assertEquals('Test Entity', $entityTransformer->transform($this->mProviderType),
            'Entity was not transformed correctly'
        );

        $entityTransformer = new EntityTransformer($this->mEntityManager, ProviderType::NAME_SPACE, 'slug');
        $this->assertEquals('test-entity', $entityTransformer->transform($this->mProviderType),
            'Entity was not transformed correctly'
        );
    }

    public function testReverseTransform()
    {
        $this->mProviderTypeRepository
            ->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValueMap([
                [['name' => 'Test Entity'], null, $this->mProviderType],
                [['slug' => 'test-entity'], null, $this->mProviderType]
            ]))
        ;

        $entityTransformer = new EntityTransformer($this->mEntityManager, ProviderType::NAME_SPACE, 'name');
        $this->assertSame($this->mProviderType, $entityTransformer->reverseTransform('Test Entity'),
            'Entity was not reverse transformed correctly'
        );

        $entityTransformer = new EntityTransformer($this->mEntityManager, ProviderType::NAME_SPACE, 'slug');
        $this->assertSame($this->mProviderType, $entityTransformer->reverseTransform('test-entity'),
            'Entity was not reverse transformed correctly'
        );

        $this->assertEquals('not-a-real-slug', $entityTransformer->reverseTransform('not-a-real-slug'),
            'Identifier was not returned after a failed reverse transformation'
        );
    }
}
