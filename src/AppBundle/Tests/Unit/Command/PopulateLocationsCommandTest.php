<?php

namespace AppBundle\Tests\Functional\Controller;

use AppBundle\Command\PopulateLocationsCommand;
use AppBundle\Entity\Address;
use AppBundle\Entity\Postcode;
use AppBundle\Repository\BaseRepository;
use AppBundle\Service\LocationService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PopulateLocationsCommandTest extends \PHPUnit_Framework_TestCase
{
    /** @var ContainerAwareCommand */
    private $command;


    public function setUp()
    {
        parent::setUp();

        $mLocationService = $this->getMockBuilder(LocationService::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $mLocationService
            ->expects($this->exactly(2))
            ->method('locateAddress')
            ->with($this->isInstanceOf(Address::NAME_SPACE))
        ;

        $mLocationService
            ->expects($this->exactly(3))
            ->method('locatePostcode')
            ->with($this->isInstanceOf(Postcode::NAME_SPACE))
        ;

        $mPostcodeRepository = $this->getMockBuilder(BaseRepository::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $mPostcodeRepository
            ->expects($this->any())
            ->method('findAll')
            ->willReturn([new Postcode(), new Postcode(), new Postcode()])
        ;

        $mAddressRepository = $this->getMockBuilder(BaseRepository::NAME_SPACE)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $mAddressRepository
            ->expects($this->any())
            ->method('findAll')
            ->willReturn([new Address(), new Address()])
        ;

        $mContainer = $this->getMockBuilder('Symfony\Component\DependencyInjection\Container')
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $mContainer
            ->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap([
                [
                    LocationService::SERVICE_NAME,
                    ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
                    $mLocationService
                ], [
                    BaseRepository::POSTCODE_REPOSITORY_SERVICE_NAME,
                    ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
                    $mPostcodeRepository
                ], [
                    BaseRepository::ADDRESS_REPOSITORY_SERVICE_NAME,
                    ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
                    $mAddressRepository
                ]
            ]));
        ;

        $application = new Application();
        $application->add(new PopulateLocationsCommand());

        $this->command = $application->find(PopulateLocationsCommand::COMMAND_NAME);
        $this->command->setContainer($mContainer);
    }

    public function testExecute()
    {
        $tester = new CommandTester($this->command);
        $tester->execute(['command' => $this->command->getName()]);
    }
}
