CrhpProviderSearchTool
======================

Steps to install:

1. git clone git@source.thomasarts.com:crystal-run/provider-lookup.git
2. composer install
3. php app/console doctrine:schema:create
4. php app/console doctrine:fixtures:load
5. php app/console application:locations:validate
6. php app/console cache:clear

To view locally:

1. php app/console server:start
2. http://localhost:8000/
